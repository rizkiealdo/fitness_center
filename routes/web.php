<?php

use App\Http\Controllers\Admin\DashboardAdminController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CheckRoleController;
use App\Http\Controllers\LaporanController;
use App\Http\Controllers\ProfilController;
use Illuminate\Support\Facades\Auth;
use App\Models\Role;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('contents.home.list');
    return redirect('/login');
});

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/check-role', [CheckRoleController::class, 'index']);


Route::get('/chekin-bulanan',[DashboardAdminController::class, 'chekinBulanan']);


Route::group(['middleware' => ['auth', 'redirectBasedOnRole']], function () {

    Route::get('/profil', [ProfilController::class, 'index']);
    Route::post('/update-foto-profil', [ProfilController::class, 'editFotoProfil']);
    Route::post('/update-password', [ProfilController::class, 'ubahPassword']);
    Route::post('/update-nama', [ProfilController::class, 'ubahNama']);

    //Laporan
    Route::get('/laporan', [LaporanController::class, 'index']);
    Route::get('/cetak', [LaporanController::class, 'cetak']);

    //Admin
    require __DIR__ . '/role1/routes-list.php';

    //Manager
    require __DIR__ . '/role2/routes-list.php';

    //Owner
    require __DIR__ . '/role3/routes-list.php';

});



