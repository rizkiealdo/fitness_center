<?php

use App\Http\Controllers\Admin\CustomerController;
use App\Http\Controllers\Admin\DashboardAdminController;
use App\Http\Controllers\Admin\DataAlatController;
use App\Http\Controllers\Admin\DataTeknisiController;
use App\Http\Controllers\Admin\HistoryController;
use App\Http\Controllers\Admin\OmsetController;
use App\Http\Controllers\Admin\OperatorController;

$namespace = 'admin';

Route::group(['middleware' => ['auth', 'isRole1']], function () {
    Route::get('/admin/dashboard', [DashboardAdminController::class, 'index'])->name('admin.dashboard');

    //Operator
    Route::get('/admin/operator', [OperatorController::class, 'index'])->name('admin.operator');
    Route::get('/admin/nota', [OperatorController::class, 'cetakNota'])->name('admin.nota');
    Route::post('/admin/store-customer', [OperatorController::class, 'storeCustomer'])->name('admin.store_customer');
    Route::post('/admin/find-customer', [OperatorController::class, 'findCustomer'])->name('admin.find-customer');
    Route::post('/admin/pilih-paket', [OperatorController::class, 'pilihPaket'])->name('admin.pilih-paket');

    //Customer
    Route::get('/admin/data-customer', [CustomerController::class, 'index'])->name('admin.data-customer');
    Route::get('/admin/data-customer/data', [CustomerController::class, 'data'])->name('admin.data-customer.data');
    Route::post('/admin/simpan-customer', [CustomerController::class, 'store'])->name('admin.simpan-customer');
    Route::delete('/admin/delete-customer', [CustomerController::class, 'delete'])->name('admin.delete-customer');

    //Transaksi
    Route::post('/admin/store-transaksi', [OperatorController::class, 'storeTransaksi'])->name('admin.store-transaksi');
    Route::get('/admin/data-transaksi', [OperatorController::class, 'dataTransaksi'])->name('admin.data-transaksi');
    Route::post('/admin/checkout', [OperatorController::class, 'checkout'])->name('admin.checkout');

    // Alat
    Route::get('/admin/data-alat', [DataAlatController::class, 'index'])->name('admin.data-alat');
    Route::get('/admin/data-alat/data', [DataAlatController::class, 'data'])->name('admin.data-alat.data');
    Route::post('/admin/store-alat', [DataAlatController::class, 'store'])->name('admin.store-alat');
    Route::delete('/admin/delete-alat', [DataAlatController::class, 'delete'])->name('admin.delete-alat');

    //Teknisi
    Route::get('/admin/data-teknisi', [DataTeknisiController::class, 'index'])->name('admin.data-teknisi');
    Route::post('/admin/store-teknisi', [DataTeknisiController::class, 'store'])->name('admin.store-teknisi');
    Route::post('/admin/kirim-pesan', [DataTeknisiController::class, 'sendWa'])->name('admin.kirim-pesan');
    Route::get('/admin/data-teknisi/data', [DataTeknisiController::class, 'data'])->name('admin.data-teknisi.data');
    Route::delete('/admin/delete-teknisi', [DataTeknisiController::class, 'delete'])->name('admin.delete-teknisi');

    // History
    Route::get('/admin/history', [HistoryController::class, 'index'])->name('admin.history');
    Route::get('/admin/history/data', [HistoryController::class, 'data'])->name('admin.history.data');

    //Omset
    Route::get('/admin/data-omset', [OmsetController::class, 'index'])->name('admin.data-omset');
    Route::get('/admin/data-omset/data', [OmsetController::class, 'data'])->name('admin.data-omset.data');

});
