<?php

use App\Http\Controllers\Admin\DashboardAdminController;
use Illuminate\Support\Facades\Route;

$namespace = 'owner';

Route::group(['middleware' => ['auth', 'isRole3']], function () {
    Route::get('/owner/dashboard', [DashboardAdminController::class, 'index'])->name('owner.dashboard');
});
