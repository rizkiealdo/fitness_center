<?php

// use App\Http\Controllers\Direktorat\DashboardController;

use App\Http\Controllers\Admin\DashboardAdminController;
use App\Http\Controllers\Manager\DashboardManagerController;
use App\Http\Controllers\Manager\ManajemenAkunController;
use App\Http\Controllers\Manager\ManajemenHargaController;
use Illuminate\Support\Facades\Route;

$namespace = 'manager';

Route::group(['middleware' => ['auth', 'isRole2']], function () {
    Route::get('/manager/dashboard', [DashboardAdminController::class, 'index'])->name('manager.dashboard');

    //Manajemen Akun
    Route::get('/manager/manajemen-akun', [ManajemenAkunController::class, 'index'])->name('manager.manajemen-akun');
    Route::get('/manager/data-akun', [ManajemenAkunController::class, 'data'])->name('manager.data-akun');
    Route::post('/manager/simpan-akun', [ManajemenAkunController::class, 'store'])->name('manager.simpan-akun');
    Route::post('/manager/tambah-akun', [ManajemenAkunController::class, 'tambah_akun'])->name('manager.tambah-akun');
    Route::delete('/manager/hapus-akun', [ManajemenAkunController::class, 'delete'])->name('manager.hapus-akun');


    //Manajemen Harga
    Route::get('/manager/manajemen-harga', [ManajemenHargaController::class, 'index'])->name('manager.manajemen-harga');
    Route::get('/manager/data-harga', [ManajemenHargaController::class, 'data'])->name('manager.data-harga');
    Route::post('/manager/simpan-harga', [ManajemenHargaController::class, 'store'])->name('manager.simpan-harga');
    Route::delete('/manager/hapus-harga', [ManajemenHargaController::class, 'delete'])->name('manager.hapus-harga');

    // Manajemen Discount
    Route::post('/manager/simpan-discount', [ManajemenHargaController::class, 'storeDiscount'])->name('manager.simpan-discount');
    Route::get('/manager/data-discount', [ManajemenHargaController::class, 'dataDiscount'])->name('manager.data-discount');
    Route::delete('/manager/hapus-discount', [ManajemenHargaController::class, 'deleteDiscount'])->name('manager.hapus-discount');
});
