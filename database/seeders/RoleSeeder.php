<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $role1 = new Role;
        $role1->name = 'Admin';
        $role1->save();

        $role2 = new Role;
        $role2->name = 'Manager';
        $role2->save();

        $role2 = new Role;
        $role2->name = 'Owner';
        $role2->save();
    }
}
