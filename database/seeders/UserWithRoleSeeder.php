<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserWithRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user = new User;
        $user->name = 'Admin';
        $user->email = 'admin@email.com';
        $user->email_verified_at = date('Y-m-d H:i:s');
        $user->password = bcrypt('Stro0ngPassword');
        $user->role_id = '1';
        $user->save();

        $user = new User;
        $user->name = 'Manager';
        $user->email = 'manager@email.com';
        $user->email_verified_at = date('Y-m-d H:i:s');
        $user->password = bcrypt('Stro0ngPassword');
        $user->role_id = '2';
        $user->save();

        $user = new User;
        $user->name = 'Owner';
        $user->email = 'owner@email.com';
        $user->email_verified_at = date('Y-m-d H:i:s');
        $user->password = bcrypt('Stro0ngPassword');
        $user->role_id = '3';
        $user->save();
    }
}
