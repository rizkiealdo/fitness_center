<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_customers', function (Blueprint $table) {
            $table->bigIncrements('id_customer');
            $table->string('is_member');
            $table->string('no_member')->nullable();
            $table->string('nim_customer')->nullable();
            $table->string('nis_customer')->nullable();
            $table->string('nama_customer');
            $table->string('no_wa_customer');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_customers');
    }
}
