<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Models\tb_paket;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\DataDiscount;

class ManajemenHargaController extends Controller
{
    //
    public function index()
    {
        return view('contents.manager.manajemen-harga.list');
    }

    public function data()
    {
        $data = tb_paket::all();

        return DataTables::of($data)
        ->addIndexColumn()
        ->make(true);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();

        try {

            $request->validate([
                'jenis_paket'       => 'required',
                'keterangan_paket'  => 'required',
                'harga_paket'       => 'required',
            ], [
                'required' => ':attribute Tidak Boleh Kosong',
                // 'dokumen.max' => 'required|mimes:pdf|max:9024',
            ]);


            tb_paket::UpdateOrCreate(
                ['id_paket' => $request->id_paket],
                [
                    'jenis_paket'       => $request->jenis_paket,
                    'keterangan_paket'  => $request->keterangan_paket,
                    'harga_paket'       => $request->harga_paket,
                ]
            );

            DB::commit();
            return response()->json(['status' => true], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['status' => false, 'msg' => $e->getMessage()], 400);
        }
    }

    public function delete(Request $request)
    {
        try {
            $id_paket = $request->id_paket;

            // Menghapus data dari tabel 'tb_paket' berdasarkan 'id_paket'
            DB::table('tb_paket')->where('id_paket', $id_paket)->delete();

            return response()->json(['status' => true, 'pesan' => 'Data berhasil dihapus']);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'pesan' => $e->getMessage()], 400);
        }

    }

    public function storeDiscount(Request $request)
    {
        DB::beginTransaction();

        try {

            $request->validate([
                'jenis_discount'    => 'required',
                'discount'          => 'required',
            ], [
                'required' => ':attribute Tidak Boleh Kosong',
                // 'dokumen.max' => 'required|mimes:pdf|max:9024',
            ]);


            DataDiscount::UpdateOrCreate(
                ['id_discount' => $request->id_discount],
                [
                    'jenis_discount'      => $request->jenis_discount,
                    'discount'  => $request->discount,
                ]
            );

            DB::commit();
            return response()->json(['status' => true], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['status' => false, 'msg' => $e->getMessage()], 400);
        }
    }

    public function dataDiscount()
    {
        $data = DataDiscount::all();

        return DataTables::of($data)
            ->addIndexColumn()
            ->make(true);
    }

    public function deleteDiscount(Request $request)
    {
        try {
            $id_discount = $request->id_discount;

            // Menghapus data dari tabel 'tb_paket' berdasarkan 'id_paket'
            DB::table('tb_discount')->where('id_discount', $id_discount)->delete();

            return response()->json(['status' => true, 'pesan' => 'Data berhasil dihapus']);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'pesan' => $e->getMessage()], 400);
        }
    }
}
