<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use App\Models\User;

class ManajemenAkunController extends Controller
{
    //
    public function index()
    {
        return view('contents.manager.manajemen-akun.list');
    }

    public function data()
    {
        $data = User::with('role')->where('role_id', '1')->orWhere('role_id', '2');

        return DataTables::of($data)
            ->addIndexColumn()
            ->make(true);

    }

    public function store(Request $request)
    {
        $request->validate([
            'id_user' => 'required',
            'role_id' => 'required',
        ], [
            'required' => ':attribute Tidak Boleh Kosong',
            // 'dokumen.max' => 'required|mimes:pdf|max:9024',
        ]);

        DB::beginTransaction();

        try {

            User::UpdateOrCreate(
                ['id' => $request->id_user],
                [
                    'role_id' => $request->role_id,
                ]
            );

            DB::commit();
            return response()->json(['status' => true], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['status' => false, 'msg' => $e->getMessage()], 400);
        }
    }

    public function tambah_akun(Request $request)
    {

        DB::beginTransaction();

        try {

            User::UpdateOrCreate(
                ['id' => $request->id_user],
                [
                    'role_id' => '1',
                    'name' => $request->name,
                    'email' => $request->email,
                    'password' => bcrypt('Stro0ngPassword')
                ]
            );

            DB::commit();
            return response()->json(['status' => true], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['status' => false, 'msg' => $e->getMessage()], 400);
        }
    }

    public function delete(Request $request)
    {
        try {
            $id_user = $request->id_user;

            // Menghapus data dari tabel 'tb_paket' berdasarkan 'id_paket'
            User::where('id', $id_user)->delete();

            return response()->json(['status' => true, 'pesan' => 'Data berhasil dihapus']);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'pesan' => $e->getMessage()], 400);
        }
    }
}
