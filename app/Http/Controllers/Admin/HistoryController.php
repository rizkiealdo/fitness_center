<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Yajra\DataTables\Facades\DataTables;
use App\Models\DataTransaksi;

class HistoryController extends Controller
{
    //
    public function index()
    {
        return view('contents.admin.history.list');
    }

    public function data()
    {
        $data = DataTransaksi::leftJoin('tb_customers', 'tb_transaksi.id_customer', 'tb_customers.id_customer')
        ->leftJoin('tb_paket', 'tb_transaksi.id_paket', 'tb_paket.id_paket')
        ->leftJoin('tb_discount', 'tb_transaksi.id_discount', 'tb_discount.id_discount')
        // ->whereDate('tb_transaksi.waktu_chekin', '!=', Carbon::today())
        ->select('tb_transaksi.*', 'tb_customers.nama_customer', 'tb_paket.jenis_paket', 'tb_discount.jenis_discount')
        ->get();

        return DataTables::of($data)
            ->addIndexColumn()
            ->make(true);
    }
}
