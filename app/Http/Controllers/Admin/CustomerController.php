<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Carbon;
use Image;
use Illuminate\Support\Facades\Storage;

//Model
use App\Models\DataCustomer;
use App\Models\tb_paket;

class CustomerController extends Controller
{
    //
    public function index()
    {
        $paket = tb_paket::all();
        $data = [
            'paket' => $paket
        ];
        return view('contents.admin.customer.list', $data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama_customer' => 'required',
            'flag_customer' => 'required',
        ], [
            'required' => ':attribute Tidak Boleh Kosong',
            // 'dokumen.max' => 'required|mimes:pdf|max:9024',
        ]);

        DB::beginTransaction();

        try {

            $tanggal_benar = date('Y-m-d', strtotime($request->member_since));
            $nilai = ['id_customer' => $request->id_customer];
            $kondisi = [
                'id_paket'          => $request->id_paket,
                'nama_customer'     => $request->nama_customer,
                'flag_customer'     => $request->flag_customer,
                'no_wa_customer'    => $request->no_wa_customer,
                'email_customer'    => $request->email_customer,
                'member_since'      => $tanggal_benar,
            ];
            $data = DataCustomer::latest()->value('id_customer');
            $no_member = $request->member_since. '/WSC/' . ($data + 1);

            if ($request->id_paket == '2' || $request->id_paket == '3') {
                $kondisi['sisa_langganan'] = '4';
                } else {
                $kondisi['sisa_langganan'] = null;
                }


            if ($request->flag_customer == 'member') {
                $kondisi['no_member'] = $no_member;
                // Membuat objek gambar dengan menggunakan template kartu member
                $image = Image::make(public_path('member_card/istockphoto-1174989482-612x612.jpg'));


                // Menambahkan logo ke gambar
                $logoPath = public_path('member_card/logo.png');
                $logo = Image::make($logoPath)->resize(100, 100); // Ubah ukuran logo sesuai kebutuhan
                $image->insert($logo, 'top-left', 20, 20); // Tambahkan logo di sudut kiri atas dengan margin 20 piksel

                // Menambahkan nomor member dan nama ke gambar
                $image->text($no_member, 325, 100, function ($font) {
                    $font->file(public_path('member_card/Helvetica-Bold.ttf'));
                    $font->size(48); // Ukuran teks lebih besar
                    $font->color('#000000'); // Warna teks putih
                    $font->align('center');
                    $font->valign('top');
                    $font->angle(0); // Sudut teks 0 derajat
                });

                $image->text('Nama :'.$request->nama_customer, 325, 150, function ($font) {
                    $font->file(public_path('member_card/Helvetica-Bold.ttf'));
                    $font->size(24); // Ukuran teks sedikit lebih kecil
                    $font->color('#000000'); // Warna teks putih
                    $font->align('center');
                    $font->valign('top');
                    $font->angle(0); // Sudut teks 0 derajat
                });

                $image->text('Member Since : '.$request->member_since, 325, 200, function ($font) {
                    $font->file(public_path('member_card/Helvetica-Bold.ttf'));
                    $font->size(24); // Ukuran teks sedikit lebih kecil
                    $font->color('#000000'); // Warna teks putih
                    $font->align('center');
                    $font->valign('top');
                    $font->angle(0); // Sudut teks 0 derajat
                });

                $image->text('Walikukun Sport Center',325, 220, function ($font) {
                    $font->file(public_path('member_card/Helvetica-Bold.ttf'));
                    $font->size(15); // Ukuran teks sedikit lebih kecil
                    $font->color('#000000'); // Warna teks putih
                    $font->align('center');
                    $font->valign('top');
                    $font->angle(0); // Sudut teks 0 derajat
                });

                $image->text('Walikukun, Ngawi, Jawa Timur, Indonesia',325, 240, function ($font) {
                    $font->file(public_path('member_card/Helvetica-Bold.ttf'));
                    $font->size(15); // Ukuran teks sedikit lebih kecil
                    $font->color('#000000'); // Warna teks putih
                    $font->align('center');
                    $font->valign('top');
                    $font->angle(0); // Sudut teks 0 derajat
                });

                // Mengubah format gambar menjadi JPG
                $image->encode('jpg');

                // Menyimpan gambar ke penyimpanan Laravel
                $folder = 'member_card/';
                $filename = '/WSC'.($data+1).'.jpg';
                $filePath = $folder . '/' . $filename;
                Storage::disk('public')->makeDirectory($folder);
                Storage::disk('public')->put($filePath, $image->stream());
            } else {
                $kondisi['no_member'] = null;
            }

            DataCustomer::updateOrCreate($nilai, $kondisi);


            DB::commit();
            return response()->json(['status' => true], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['status' => false, 'msg' => $e->getMessage()], 400);
        }
    }

    public function data(Request $request)
    {
        // $flagCustomer = $request->input('flag_customer');

        $data = DataCustomer::get();

        return DataTables::of($data)
            ->addIndexColumn()
            ->make(true);
    }


    public function delete(Request $request)
    {
        try {
            $id_customer = $request->id_customer;

            // Menghapus data dari tabel 'tb_paket' berdasarkan 'id_paket'
            DataCustomer::where('id_customer', $id_customer)->delete();

            return response()->json(['status' => true, 'pesan' => 'Data berhasil dihapus']);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'pesan' => $e->getMessage()], 400);
        }
    }
}
