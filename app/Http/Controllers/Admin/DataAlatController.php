<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DataAlat;
use App\Models\DataTeknisi;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class DataAlatController extends Controller
{

    public function index()
    {
        $teknisi = DataTeknisi::all();

        $list = [
            'teknisi' => $teknisi
        ];

        return view('contents.admin.alat.list', $list);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();

        try {

            DataAlat::updateOrCreate(
                ['id_alat' => $request->id_alat],
                [
                    'nama_alat' => $request->nama_alat,
                    'nomor_seri_alat' => $request->nomor_seri_alat,
                    'id_teknisi' => $request->id_teknisi,
                    ]
            );

            DB::commit();
            return response()->json(['status' => true], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['status' => false, 'msg' => $e->getMessage()], 400);
        }
    }

    public function data()
    {
        $data = DataAlat::with('teknisi')->get();

        return DataTables::of($data)
            ->addIndexColumn()
            ->make(true);
    }

    public function delete(Request $request)
    {
        try {
            $id_alat = $request->id_alat;

            // Menghapus data dari tabel 'tb_paket' berdasarkan 'id_paket'
            DataAlat::where('id_alat', $id_alat)->delete();

            return response()->json(['status' => true, 'pesan' => 'Data berhasil dihapus']);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'pesan' => $e->getMessage()], 400);
        }
    }
}
