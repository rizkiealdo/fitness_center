<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DataTransaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OmsetController extends Controller
{
    //
    public function index()
    {
        $data = $this->data();

        $all = [
            'data' => $data
        ];

        return view('contents.admin.omset.list', $all);
    }

    public function data()
    {
        $data = DataTransaksi::select(
            // "id_transaksi",
            DB::raw("(sum(nominal_transaksi)) as omset"),
            DB::raw("(DATE_FORMAT(created_at, '%M-%Y')) as month_year"),
            DB::raw("(avg(nominal_transaksi)) as rata_rata"),
        )
            ->orderBy('created_at', 'desc')
            ->groupBy(DB::raw("DATE_FORMAT(created_at, '%M-%Y')"))
            ->get();

        return $data;
    }
}
