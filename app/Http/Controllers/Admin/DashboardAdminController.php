<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DataCustomer;
use App\Models\DataTransaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardAdminController extends Controller
{
    //
    public function index()
    {
        $data = DataCustomer::all();

        $total = $data->count();
        $member = $data->where('flag_customer','member')->count();
        $non_member = $data->where('flag_customer', 'non-member')->count();

        $harian = DataTransaksi::select(
            DB::raw("(AVG('id_customer')) as harian"),
        )->groupBy(DB::raw("id_customer"))->count();

        $list = [
            'total' => $total,
            'member' => $member,
            'non_member' => $non_member,
            'harian' => $harian,
        ];
        return view('contents.admin.dashboard.list', $list);
    }

    public function chekinBulanan()
    {
        $data = DataTransaksi::select(
            // "id_transaksi",
            DB::raw("(count(DATE_FORMAT(created_at, '%M'))) as jumlah_bulanan"),
            DB::raw("(DATE_FORMAT(created_at, '%M')) as bulan"),
        )
            ->orderBy('created_at', 'desc')
            ->groupBy(DB::raw("DATE_FORMAT(created_at, '%M')"))
            ->get();

        // Mengembalikan response JSON
        return response()->json($data);
    }

    private function dataMember()
    {
        $data = DataTransaksi::select(
            DB::raw("(AVG('id_customer')) as harian"),
        )
            ->groupBy(DB::raw("id_customer"))
            ->get();

        return $data;
    }
}
