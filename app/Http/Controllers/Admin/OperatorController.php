<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DataTransaksi;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Carbon;
use Barryvdh\DomPDF\PDF as DomPDF;


//model
use App\Models\DataCustomer;
use App\Models\DataDiscount;
use App\Models\tb_paket;


class OperatorController extends Controller
{
    //
    public function index()
    {
        $paket      = tb_paket::get();

        $data = [
            'paket'     => $paket
        ];

        return view('contents.admin.operator.list', $data);
    }

    public function dataTransaksi()
    {
        $data = DataTransaksi::leftJoin('tb_customers', 'tb_transaksi.id_customer', 'tb_customers.id_customer')
            ->leftJoin('tb_paket', 'tb_transaksi.id_paket', 'tb_paket.id_paket')
            ->leftJoin('tb_discount', 'tb_transaksi.id_discount', 'tb_discount.id_discount')
            ->whereDate('tb_transaksi.waktu_chekin', Carbon::today()) // Memfilter data dengan waktu checkin hari ini
            ->select('tb_transaksi.*', 'tb_customers.nama_customer', 'tb_paket.jenis_paket', 'tb_discount.jenis_discount')
            ->get();

        $all = [
            'data' => $data
        ];

        return view('contents.admin.operator.table', $all);
    }

    private function dataDiscount()
    {
        $data = DataDiscount::all();

        return $data;
    }

    public function findCustomer(Request $request)
    {
        try {
            $no_member = $request->no_member;

            $data = DataCustomer::where('no_member', $no_member)->first();

            // dd($data);
            return response()->json($data);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'pesan' => $e->getMessage()], 400);
        }
    }

    public function pilihPaket(Request $request)
    {
        $id_paket = $request->id_paket;

        if ($id_paket == 1) {
            $customer = DataCustomer::where('id_paket', $id_paket)->get();
            $discount = $this->dataDiscount();

            $data = [
                'customer' => $customer,
                'discount' => $discount
            ];
            return view('contents.admin.operator.modal.form.form-harian')->with($data);
        } elseif ($id_paket == 2) {
            $customer = DataCustomer::where('id_paket', $id_paket)->get();
            $discount = $this->dataDiscount();

            $data = [
                'customer' => $customer,
                'discount' => $discount
            ];
            return view('contents.admin.operator.modal.form.form-bulanan')->with($data);
        } elseif ($id_paket == 3) {
            $customer = DataCustomer::where('id_paket', $id_paket)->get();
            $discount = $this->dataDiscount();

            $data = [
                'customer' => $customer,
                'discount' => $discount
            ];
            return view('contents.admin.operator.modal.form.form-trainer')->with($data);
        }
    }

    public function storeTransaksi(Request $request)
    {
        DB::beginTransaction();
        try {


            $validator = Validator::make($request->all(), [
                'id_customer' => 'required',
                'id_paket' => 'required',
            ]);

            if ($request->id_paket == '1') {
                $transaksi = new DataTransaksi();
                $transaksi->id_customer = $request->id_customer;
                $transaksi->id_paket = $request->id_paket;
                $transaksi->id_discount = $request->id_discount;
                $transaksi->waktu_chekin = Carbon::now()->setTimezone('Asia/Jakarta');
                $transaksi->save();
            } else if ($request->id_paket == '2' || $request->id_paket == '3') {
                $customer = DataCustomer::where('id_customer', $request->id_customer)->first();
                if ($customer->sisa_langganan == 0 || $customer->sisa_langganan <= 0) {
                    return response()->json(['status' => false, 'msg' => 'Masa Langganan Sudah Berakhir'], 200);
                } else {
                    $transaksi = new DataTransaksi();
                    $transaksi->id_customer = $request->id_customer;
                    $transaksi->id_paket = $request->id_paket;
                    $transaksi->id_discount = $request->id_discount;
                    $transaksi->waktu_chekin = Carbon::now()->setTimezone('Asia/Jakarta');

                    $customer->sisa_langganan = $customer->sisa_langganan-1;
                    $customer->save();
                    $transaksi->save();
                }
            }

            DB::commit();
            return response()->json(['status' => true], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['status' => false, 'msg' => $e->getMessage()], 400);
        }
    }

    public function checkout(Request $request)
    {
        DB::beginTransaction();
        try {


            $validator = Validator::make($request->all(), [
                'id_transaksi' => 'required',
            ]);

            $checkout = DataTransaksi::where('id_transaksi', $request->id_transaksi)->first();
            if ($checkout->id_paket == '1') {

                $harga_paket = tb_paket::where('id_paket', $checkout->id_paket)->value('harga_paket');
                $discount = DataDiscount::where('id_discount', $checkout->id_paket)->value('discount');
                $total_discount = $harga_paket * ($discount / 100);
                $total_harga = $harga_paket - $total_discount;

                $durasi = $request->durasi;
                $denda = ($durasi >= 105) ? ($durasi - 90) * 1000 : 0;

                if ($checkout->id_discount == null) {
                    $checkout->nominal_transaksi = $harga_paket + $denda;
                } else {
                    $checkout->nominal_transaksi = $total_harga + $denda;
                }

                $checkout->denda = $denda;
                $checkout->durasi = ($durasi >= 105) ? $durasi : 90;
                $checkout->waktu_chekout = Carbon::now()->setTimezone('Asia/Jakarta');
                $checkout->save();
            } else if ($checkout->id_paket == '2' || $checkout->id_paket == '3') {
                $customer = DataCustomer::where('id_customer', $checkout->id_customer)->first();

                if ($customer->sisa_langganan >= 3) {
                    $checkout = DataTransaksi::where('id_transaksi', $request->id_transaksi)->first();
                    $harga_paket = tb_paket::where('id_paket', $checkout->id_paket)->value('harga_paket');
                    $discount = DataDiscount::where('id_discount', $checkout->id_paket)->value('discount');
                    $total_discount = $harga_paket * ($discount / 100);
                    $total_harga = $harga_paket - $total_discount;

                    $durasi = $request->durasi;
                    $denda = ($durasi >= 105) ? ($durasi - 90) * 1000 : 0;

                    if ($checkout->id_discount == null) {
                        $checkout->nominal_transaksi = $harga_paket + $denda;
                    } else {
                        $checkout->nominal_transaksi = $total_harga + $denda;
                    }

                    $checkout->denda = $denda;
                    $checkout->durasi = ($durasi >= 105) ? $durasi : 90;
                    $checkout->waktu_chekout = Carbon::now()->setTimezone('Asia/Jakarta');
                    $checkout->save();
                } else if ($customer->sisa_langganan <= 3) {
                    $checkout = DataTransaksi::where('id_transaksi', $request->id_transaksi)->first();
                    $harga_paket = '0';

                    $durasi = $request->durasi;
                    $denda = ($durasi >= 105) ? ($durasi - 90) * 1000 : 0;

                    $checkout->nominal_transaksi = $harga_paket + $denda;

                    $checkout->denda = $denda;
                    $checkout->durasi = ($durasi >= 105) ? $durasi : 90;
                    $checkout->waktu_chekout = Carbon::now()->setTimezone('Asia/Jakarta');
                    $checkout->save();
                }

            }

            DB::commit();
            return response()->json(['status' => true, 'pesan' => 'Berhasil Checkout'], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['status' => false, 'pesan' => $e->getMessage()], 400);
        }
    }

    public function cetakNota(Request $request)
    {
        $id_transaksi = $request->id_transaksi;

        $data = DataTransaksi::where('id_transaksi', $id_transaksi)
        ->with('customer')->with('paket')->with('discount')->first();

        // Buat objek Dompdf
        $pdf = app(DomPDF::class);

        $customPaper = array(0, 0, 350, 500);

        // Render view 'contents.laporan.cetak' dengan data yang sesuai
        $pdf->loadView('contents.admin.operator.cetak', ['data' => $data])->setPaper($customPaper, 'portrait');

        // Mengembalikan PDF sebagai respons langsung
        return $pdf->stream('Nota Transaksi '.$id_transaksi.'.pdf');
    }
}
