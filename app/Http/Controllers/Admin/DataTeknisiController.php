<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DataTeknisi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class DataTeknisiController extends Controller
{
    //
    public function index()
    {
        return view('contents.admin.teknisi.list');
    }

    public function store(Request $request)
    {
        DB::beginTransaction();

        try {

            DataTeknisi::updateOrCreate(
                ['id_teknisi' => $request->id_teknisi],
                [
                    'nama_teknisi' => $request->nama_teknisi,
                    'nomor_wa_teknisi' => $request->nomor_wa_teknisi,
                ]
            );

            DB::commit();
            return response()->json(['status' => true], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['status' => false, 'msg' => $e->getMessage()], 400);
        }
    }

    public function data()
    {
        $data = DataTeknisi::all();

        return DataTables::of($data)
            ->addIndexColumn()
            ->make(true);
    }

    public function delete(Request $request)
    {
        try {
            $id_teknisi = $request->id_teknisi;

            // Menghapus data dari tabel 'tb_paket' berdasarkan 'id_paket'
            DataTeknisi::where('id_teknisi', $id_teknisi)->delete();

            return response()->json(['status' => true, 'pesan' => 'Data berhasil dihapus']);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'pesan' => $e->getMessage()], 400);
        }
    }

    public function sendWa(Request $request)
    {
        try {
            $nomor_wa = $request->nomor_wa;
            $pesan_teknisi = $request->pesan_teknisi;

            $send = "https://wa.me/" . $nomor_wa . "?text=" . urlencode($pesan_teknisi);

            // Mengarahkan pengguna ke tautan WhatsApp untuk mengirim pesan
            return redirect()->away($send);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'pesan' => $e->getMessage()], 400);
        }
    }

}
