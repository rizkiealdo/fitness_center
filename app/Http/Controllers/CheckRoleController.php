<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class CheckRoleController extends Controller
{
    //
    public function index()
    {
        if (Gate::check('isRole1')) {
            return redirect()->route('admin.dashboard');
        } elseif (Gate::check('isRole2')) {
            return redirect()->route('manager.dashboard');
        } elseif (Gate::check('isRole3')) {
            return redirect()->route('owner.dashboard');
        } else {
            // Jika user tidak memiliki role yang sesuai, lakukan sesuatu
            url('/login');
        }
    }
}
