<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\DataTransaksi;
use Barryvdh\DomPDF\PDF as DomPDF;

class LaporanController extends Controller
{
    //
    public function index()
    {
        $data = $this->data();

        $list = [
            'data' => $data
        ];

        return view('contents.laporan.list',$list);
    }

    private function data()
    {
        $data = DataTransaksi::select(
            // "id_transaksi",
            DB::raw("(sum(nominal_transaksi)) as omset"),
            DB::raw("(DATE_FORMAT(waktu_chekout, '%M-%Y')) as month_year"),
            DB::raw("(avg(nominal_transaksi)) as rata_rata"),
            DB::raw("(count(id_transaksi)) as jumlah_transaksi"),
        )
            ->orderBy('waktu_chekout', 'desc')
            ->groupBy(DB::raw("DATE_FORMAT(waktu_chekout, '%M-%Y')"))
            ->get();

        return $data;
    }

    public function cetak(Request $request)
    {
        $bulan_tahun = $request->input('bulan_tahun');

        // Proses untuk mendapatkan data sesuai dengan bulan_tahun, Anda dapat menyesuaikan dengan kebutuhan Anda.
        $data = DataTransaksi::with('customer')
        ->with('discount')
        ->with('paket')
        ->where(DB::raw("DATE_FORMAT(waktu_chekout, '%M-%Y')"), $bulan_tahun)
            ->get();

        // Buat objek Dompdf
        $pdf = app(DomPDF::class);

        // Render view 'contents.laporan.cetak' dengan data yang sesuai
        $pdf->loadView('contents.laporan.cetak', ['data' => $data]);

        // Mengembalikan PDF sebagai respons langsung
        return $pdf->stream('laporan-bulanan-pdf.pdf');
    }

}
