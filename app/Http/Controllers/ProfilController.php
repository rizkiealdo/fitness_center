<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

use App\Models\User;

class ProfilController extends Controller
{
    //
    public function index()
    {
        return view('contents.profil');
    }

    public function editFotoProfil(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_user' => 'required',
            // 'foto_profil' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        if (!$validator->fails()) {
            DB::beginTransaction();
            try {

                // Ambil data pengguna berdasarkan ID
                $user = User::find($request->id_user);

                // Mengunggah foto ke penyimpanan
                $path = $request->file('foto_profil')->store('public/foto_profil');

                // Ambil nama file dari path
                $namaFile = basename($path);

                // Bentuk path lengkap dengan menambahkan '/foto_profil/' di depan nama file
                $pathLengkap = 'foto_profil/' . $namaFile;
                $user->foto_profil = $pathLengkap;
                $user->save();

                DB::commit();
                return response()->json(['status' => true], 200);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(['status' => false, 'msg' => $e->getMessage()], 400);
            }
        } else {
            $response = [
                'status' => false,
                'errors' => $validator->errors()
            ];
            return response()->json(['status' => false, 'msg' => $response], 400);
        }
    }

    public function ubahPassword(Request $request)
    {
        DB::beginTransaction();
        try {
            $id_user = Auth::user()->id;

            $password_baru = $request->password_baru;

            $user = User::find($id_user);
            $user->password = bcrypt($password_baru);
            $user->save();
            DB::commit();
            return response()->json(['status' => true], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['status' => false, 'msg' => $e->getMessage()], 400);
        }
    }

    public function ubahNama(Request $request)
    {
        DB::beginTransaction();
        try {
            $id_user = Auth::user()->id;

            $nama_baru = $request->nama_baru;

            $user = User::find($id_user);
            $user->name = $nama_baru;
            $user->save();
            DB::commit();
            return response()->json(['status' => true], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['status' => false, 'msg' => $e->getMessage()], 400);
        }
    }
}
