<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle($request, Closure $next, ...$roles)
    {
        if (!Auth::check()) {
            // Jika pengguna belum login, redirect ke halaman login
            return redirect('/login');
        }

        $user = Auth::user();

        if (in_array($user->role_id, $roles)) {
            // Jika pengguna memiliki role yang diperbolehkan, lanjutkan request
            return $next($request);
        }

        // Jika pengguna tidak memiliki role yang diperbolehkan, redirect ke halaman home
        return redirect('/home');
    }
}
