<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataFoto extends Model
{
    use HasFactory;
    protected $table = 'tb_foto'; // Menyebutkan nama tabel yang sesuai

    protected $primaryKey = 'id_foto'; // Menyebutkan nama kolom primary key yang sesuai

    protected $fillable = [
        'path',
        'jenis_foto',
        'id_alat',
    ];
}
