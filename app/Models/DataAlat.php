<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class DataAlat extends Model
{
    use HasFactory;
    protected $table = 'tb_alat'; // Menyebutkan nama tabel yang sesuai

    protected $primaryKey = 'id_alat'; // Menyebutkan nama kolom primary key yang sesuai

    protected $fillable = [
        'nama_alat',
        'nomor_seri_alat',
        'id_teknisi',
    ];

    public function teknisi() : HasMany
    {
        return $this->hasMany(DataTeknisi::class, 'id_teknisi', 'id_teknisi');
    }
}
