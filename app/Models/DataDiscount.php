<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DataDiscount extends Model
{
    use HasFactory;
    protected $table = 'tb_discount'; // Menyebutkan nama tabel yang sesuai

    protected $primaryKey = 'id_discount'; // Menyebutkan nama kolom primary key yang sesuai

    protected $fillable=[
        'jenis_discount',
        'discount'
    ];
}
