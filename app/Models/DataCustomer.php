<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class DataCustomer extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'tb_customers'; // Menyebutkan nama tabel yang sesuai

    protected $primaryKey = 'id_customer'; // Menyebutkan nama kolom primary key yang sesuai

    protected $fillable = [
        'is_member',
        'id_paket',
        'flag_customer',
        'no_member',
        'nama_customer',
        'no_wa_customer',
        'email_customer',
        'member_since',
        'sisa_langganan',
    ];

    // Jika Anda ingin menggunakan timestamps created_at dan updated_at
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
