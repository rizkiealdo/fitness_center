<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataTransaksi extends Model
{
    use HasFactory;
    protected $table = 'tb_transaksi'; // Menyebutkan nama tabel yang sesuai

    protected $primaryKey = 'id_transaksi'; // Menyebutkan nama kolom primary key yang sesuai

    protected $fillable = [
        'id_customer',
        'id_paket',
        'waktu_checkin',
        'waktu_chekout',
        'durasi',
        'denda',
        'nominal_transaksi',
    ];

    // Jika Anda ingin menggunakan timestamps created_at dan updated_at
    public $timestamps = true;

    public function customer()
    {
        return $this->hasOne(DataCustomer::class, 'id_customer', 'id_customer');
    }

    public function paket()
    {
        return $this->hasOne(tb_paket::class, 'id_paket', 'id_paket');
    }

    public function discount()
    {
        return $this->hasOne(DataDiscount::class, 'id_discount', 'id_discount');
    }
}
