<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tb_paket extends Model
{
    use HasFactory;
    protected $table = 'tb_paket'; // Menyebutkan nama tabel yang sesuai

    protected $primaryKey = 'id_paket'; // Menyebutkan nama kolom primary key yang sesuai

    protected $fillable = [
        'jenis_paket',
        'keterangan_paket',
        'harga_paket',
    ];

    // Jika Anda ingin menggunakan timestamps created_at dan updated_at
    public $timestamps = true;
}
