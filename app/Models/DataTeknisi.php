<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataTeknisi extends Model
{
    use HasFactory;
    protected $table = 'tb_teknisi'; // Menyebutkan nama tabel yang sesuai

    protected $primaryKey = 'id_teknisi'; // Menyebutkan nama kolom primary key yang sesuai

    protected $fillable = [
        'nama_teknisi',
        'nomor_wa_teknisi',
    ];

    public function alat()
    {
        return $this->belongsTo(DataAlat::class, 'id_teknisi', 'id_teknisi');
    }
}
