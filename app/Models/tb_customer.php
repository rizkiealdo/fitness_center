<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class tb_customer extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'tb_customers'; // Menyebutkan nama tabel yang sesuai

    protected $primaryKey = 'id_customer'; // Menyebutkan nama kolom primary key yang sesuai

    protected $fillable = [
        'is_member',
        'no_member',
        'nim_customer',
        'nis_customer',
        'nama_customer',
        'no_wa_customer',
    ];

    // Jika Anda ingin menggunakan timestamps created_at dan updated_at
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
