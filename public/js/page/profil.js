$(document).ready(function () {
    $('#form-foto-profil').on('submit', function (e) {
        e.preventDefault(); // mencegah form untuk me-reload halaman
        var formData = new FormData(this);

        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,

        }).done((res) => {
            if (res.status) {
                Swal.fire({
                    icon: 'success',
                    title: 'Yess!!',
                    text: 'Data Berhasil Disimpan!'
                });
                location.reload();
            } else {
                Swal.fire({
                    title: 'Gagal',
                    text: res.mssg,
                    icon: 'error'
                });
            }
        });
    });

    $('#form-password').on('submit', function (e) {
        e.preventDefault();

        var formData = new FormData(this);

        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
        }).done(function (response) {
            if (response.status) {
                iziToast.success({
                    title: 'OK',
                    icon: 'icon-success',
                    position: 'topCenter',
                    message: 'Data Berhasil Disimpan',
                });
                table_data.ajax.reload();
                table_non_member.ajax.reload();
                $('#form-customer')[0].reset();
            } else {
                Swal.fire({
                    title: 'Gagal',
                    text: res.mssg,
                    icon: 'error'
                });
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            Swal.fire({
                title: 'Gagal',
                text: 'Terjadi kesalahan saat mengirim data',
                icon: 'error'
            });
        });
    });

    $('#form-nama').on('submit', function (e) {
        e.preventDefault();

        var formData = new FormData(this);

        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
        }).done(function (response) {
            if (response.status) {
                iziToast.success({
                    title: 'OK',
                    icon: 'icon-success',
                    position: 'topCenter',
                    message: 'Data Berhasil Disimpan',
                });
                location.reload();
                $('#form-nama')[0].reset();
            } else {
                Swal.fire({
                    title: 'Gagal',
                    text: res.mssg,
                    icon: 'error'
                });
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            Swal.fire({
                title: 'Gagal',
                text: 'Terjadi kesalahan saat mengirim data',
                icon: 'error'
            });
        });
    });

});
