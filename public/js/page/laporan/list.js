$(document).ready(function () {
    $('.btn-cetak').click(function (e) {
        // e.preventDefault();
        var bulan_tahun = $(this).data('bulan');
        let token = document.head.querySelector('meta[name="csrf-token"]').content;

        $.ajax({
            url: "/cetak",
            type: 'POST',
            // dataType: 'JSON',
            data: {
                bulan_tahun: bulan_tahun,
                _token: token
            },
            success: function (response) {
                // Membuka PDF dalam tab baru jika ada
                if (response.pdfContent) {
                    var pdfWindow = window.open("", "_blank");
                    pdfWindow.document.write(response.pdfContent);
                }
            },
            error: function (xhr, status, error) {
                // Tangani kesalahan jika ada
                console.error(error);
            }
        });
    });
});
