$(document).ready(function () {
    table_data = $('#table-akun').DataTable({
        processing: true,
        serverSide: true,
        processing: true,
        theme: 'bootstrap',
        ajax: {
            url: "/manager/data-akun",
            type: 'get',
            dataType: 'json'
        },
        order: [4, 'desc'],
        columnDefs: [{
            targets: [0],
            searchable: false,
            orderable: false,
            className: 'text-center'
        },
        ],
        columns: [{
            data: 'DT_RowIndex'
        }, {
            data: 'name'
        },
        {
            data: 'email'
        }, {
            data: 'role.name'
        }, {
            data: 'id',
            render: (data, type, row) => {
                const button_plotting = $('<button>', {
                    class: 'btn btn-outline-primary btn-plotting',
                    html: '<i class="bx bx-check-shield"></i></i>',
                    'data-id': data,
                    title: 'Ubah Role',
                    'data-placement': 'top',
                    'data-toggle': 'tooltip'
                });

                const button_delete = $('<button>', {
                    class: 'btn btn-outline-danger btn-delete',
                    html: '<i class="bx bxs-trash"></i>',
                    'data-id': data,
                    title: 'Hapus Data',
                    'data-placement': 'top',
                    'data-toggle': 'tooltip'
                });

                return $('<div>', {
                    class: 'btn-group',
                    html: () => {
                        let arr = [];
                        arr.push(button_plotting, button_delete)
                        return arr;
                    }
                }).prop('outerHTML');
            }
        }]
    });

    $('#form-plotting').on('submit', function (e) {

        e.preventDefault(); // mencegah form untuk me-reload halaman
        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: $(this).serialize(),
            dataType: 'json',

        }).done((res) => {
            if (res.status) {
                iziToast.success({
                    title: 'OK',
                    icon: 'icon-success',
                    position: 'topCenter',
                    message: 'Data Berhasil Disimpan',
                });
                this.reset();
                table_data.ajax.reload();
            } else {
                Swal.fire({
                    title: 'Gagal',
                    text: res.mssg,
                    icon: 'error'
                });
            }
        });
    });

    $('#table-akun').on('click', '.btn-plotting', function () {
        let data_row = table_data.row($(this).closest('tr')).data();
        $('#id_user').val(data_row.id);
        $('#role_id').val(data_row.role_id);
        $('#modal-plotting').modal('show');
    });

    $('#table-akun').on('click', '.btn-delete', function () {
        let data = table_data.row($(this).closest('tr')).data();
        let {
            id_discount
        } = data;
        let token = document.head.querySelector('meta[name="csrf-token"]').content;
        Swal.fire({
            title: "Anda yakin menghapus data ini?",
            text: "Data yang sudah terhapus tidak bisa dikembalikan!",
            icon: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#34c38f",
            cancelButtonColor: "#f46a6a",
            confirmButtonText: "Ya, Hapus!",
            cancelButtonText: "Batal"
        }).then((t) => {
            if (t.isConfirmed) {
                $.ajax({
                    // url: "{{ route('superadmin.produk.delete') }}",
                    url: "/manager/hapus-akun",
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        server: data.id,
                        _method: 'DELETE',
                        _token: token
                    },
                }).done((res) => {
                    if (res.status) {
                        Swal.fire({
                            title: 'Sukses',
                            text: res.pesan,
                            icon: 'success'
                        });
                        table_data.ajax.reload();
                    } else {
                        Swal.fire({
                            title: 'Oops..!',
                            text: res.pesan,
                            icon: 'error'
                        });
                    }
                })
            }
        })
    });


    $('#btn-tambah-akun').on('click', function(){
        $('#modal-tambah_akun').modal('show');
    });

    $('#form-tambah-akun').on('submit', function (e) {

        e.preventDefault(); // mencegah form untuk me-reload halaman
        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: $(this).serialize(),
            dataType: 'json',

        }).done((res) => {
            if (res.status) {
                iziToast.success({
                    title: 'OK',
                    icon: 'icon-success',
                    position: 'topCenter',
                    message: 'Data Berhasil Disimpan',
                });
                this.reset();
                table_data.ajax.reload();
            } else {
                Swal.fire({
                    title: 'Gagal',
                    text: res.mssg,
                    icon: 'error'
                });
            }
        });
    });

});
