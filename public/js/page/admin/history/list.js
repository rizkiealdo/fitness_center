$(document).ready(function () {
    table_data = $('#table-history').DataTable({
        processing: true,
        theme: 'bootstrap',
        ajax: {
            url: "/admin/history/data",
            type: 'get',
            dataType: 'json',
        },
        order: [],
        columnDefs: [{
            targets: [0],
            searchable: false,
            orderable: false,
            className: 'text-center'
        },
        ],
        columns: [{
            data: 'DT_RowIndex'
        }, {
            data: 'nama_customer'
        },
        {
            data: 'jenis_paket',
        },
        {
            data: 'jenis_discount'
        }, {
            data: 'waktu_chekin'
        }, {
            data: 'durasi'
        }, {
            data: 'waktu_chekout',
        }, {
            data: 'nominal_transaksi',
        }]
    });
});
