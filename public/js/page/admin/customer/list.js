$(document).ready(function () {
    var baseUrl = window.location.origin;

    $('#btn-tambah').click(function (e) {
        e.preventDefault();

        $('#modal-customer').modal('show');
    });

    $('#flag_customer').change(function (e) {
        e.preventDefault();

        if ($('#flag_customer').val() === 'member') {
            $('.member').removeAttr('hidden');
        } else if ($('#flag_customer').val() === 'non-member') {
            $('.member').attr('hidden', 'hidden');
        }
    });

    $('#form-customer').on('submit', function (e) {
        e.preventDefault();

        var formData = new FormData(this);

        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
        }).done(function (response) {
            if (response.status) {
                iziToast.success({
                    title: 'OK',
                    icon: 'icon-success',
                    position: 'topCenter',
                    message: 'Data Berhasil Disimpan',
                });
                table_data.ajax.reload();
                table_non_member.ajax.reload();
                $('#form-customer')[0].reset();
            } else {
                Swal.fire({
                    title: 'Gagal',
                    text: res.mssg,
                    icon: 'error'
                });
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            Swal.fire({
                title: 'Gagal',
                text: 'Terjadi kesalahan saat mengirim data',
                icon: 'error'
            });
        });
    });

    table_data = $('#table-member').DataTable({
        processing: true,
        serverSide: true,
        processing: true,
        theme: 'bootstrap',
        ajax: {
            url: "/admin/data-customer/data",
            type: 'get',
            dataType: 'json',
        },
        order: [6, 'desc'],
        columnDefs: [{
            targets: [0],
            searchable: false,
            orderable: false,
            className: 'text-center'
        },
        ],
        columns: [{
            data: 'DT_RowIndex'
        }, {
            data: 'id_soal',
        },
            {
                data: 'flag_customer',
                render: function (data, type, row) {
                    // Menampilkan flag_customer dalam elemen <span> dengan kelas Bootstrap 4
                    let bgColorClass = '';

                    if (data === 'member') {
                        bgColorClass = 'bg-primary';
                    } else if (data === 'non-member') {
                        bgColorClass = 'bg-danger';
                    } else {
                        bgColorClass = 'bg-danger';
                    }

                    return '<span class="badge ' + bgColorClass + ' text-white">' + data + '</span>';
                }
            },
        {
            data: 'member_since'
        }, {
            data: 'no_wa_customer'
        },  {
            data: 'email_customer'
        }, {
            data: 'id_customer',
            render: function (data, type, row) {
                // Membuat kondisi if untuk menampilkan tombol hanya ketika flag_customer adalah 'member'
                if (row.flag_customer === 'member') {
                    const button_member = $('<button>', {
                        class: 'btn btn-outline-primary btn-member',
                        html: '<i class="bx bxs-file-jpg"></i>',
                        'data-id': data,
                        title: 'Ubah Role',
                        'data-placement': 'top',
                        'data-toggle': 'tooltip'
                    });

                    return $('<div>', {
                        class: 'btn-group',
                        html: () => {
                            let arr = [];
                            arr.push(button_member);
                            return arr;
                        }
                    }).prop('outerHTML');
                } else {
                    return ''; // Mengembalikan tampilan kosong ketika flag_customer bukan 'member'
                }
            }
        }, {
            data: 'id_customer',
            render: (data, type, row) => {
                const button_edit = $('<button>', {
                    class: 'btn btn-outline-primary btn-edit',
                    html: '<i class="bx bx-edit" ></i>',
                    'data-id': data,
                    title: 'Ubah Role',
                    'data-placement': 'top',
                    'data-toggle': 'tooltip'
                });

                const button_delete = $('<button>', {
                    class: 'btn btn-outline-danger btn-delete',
                    html: '<i class="bx bxs-trash"></i>',
                    'data-id': data,
                    title: 'Hapus Data',
                    'data-placement': 'top',
                    'data-toggle': 'tooltip'
                });

                return $('<div>', {
                    class: 'btn-group',
                    html: () => {
                        let arr = [];
                        arr.push(button_edit, button_delete)
                        return arr;
                    }
                }).prop('outerHTML');
            }
        }]
    });

    $('#table-member').on('click', '.btn-member', function () {
        // let data_row = table.row($(this).closest('tr')).data();
        let data_row = table_data.row($(this).closest('tr')).data();
        var imageSrc = $(this).attr('data-id');
        $('#imagePreview').attr('src', baseUrl + '/storage/member_card/WSC' + imageSrc + '.jpg');
        $('#imageModal').modal('show');
    });

    $('#table-member').on('click', '.btn-edit', function () {
        // let data_row = table.row($(this).closest('tr')).data();
        let data_row = table_data.row($(this).closest('tr')).data();
        $('.id_customer').val(data_row.id_customer);
        $('#nama_customer').val(data_row.nama_customer);
        $('#no_wa_customer').val(data_row.no_wa_customer);
        $('#flag_customer').val(data_row.flag_customer).trigger('change');
        $('#email_customer').val(data_row.email_customer);
        $('#modal-customer').modal('show');
    });

    $('#modal-customer').on('hidden.bs.modal', function (e) {
        $('.member').attr('hidden', 'hidden');
        $(this).find('#form-customer')[0].reset();
    });

    $('#table-member').on('click', '.btn-delete', function () {
        let data = table_data.row($(this).closest('tr')).data();
        let {
            id_discount
        } = data;
        let token = document.head.querySelector('meta[name="csrf-token"]').content;
        Swal.fire({
            title: "Anda yakin menghapus data ini?",
            text: "Data yang sudah terhapus tidak bisa dikembalikan!",
            icon: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#34c38f",
            cancelButtonColor: "#f46a6a",
            confirmButtonText: "Ya, Hapus!",
            cancelButtonText: "Batal"
        }).then((t) => {
            if (t.isConfirmed) {
                $.ajax({
                    // url: "{{ route('superadmin.produk.delete') }}",
                    url: "/admin/delete-customer",
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        id_customer: data.id_customer,
                        _method: 'DELETE',
                        _token: token
                    },
                }).done((res) => {
                    if (res.status) {
                        Swal.fire({
                            title: 'Sukses',
                            text: res.pesan,
                            icon: 'success'
                        });
                        table_data.ajax.reload();
                    } else {
                        Swal.fire({
                            title: 'Oops..!',
                            text: res.pesan,
                            icon: 'error'
                        });
                    }
                })
            }
        })
    });


    $("#member_since").flatpickr({
        dateFormat: "d-m-Y",
    });
});
