$(document).ready(function () {
    $('#form-teknisi').on('submit', function (e) {
        e.preventDefault();

        var formData = new FormData(this);

        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
        }).done(function (response) {
            if (response.status) {
                iziToast.success({
                    title: 'OK',
                    icon: 'icon-success',
                    position: 'topCenter',
                    message: 'Data Berhasil Disimpan',
                });
                table_data.ajax.reload();
                $('#form-teknisi')[0].reset();
            } else {
                Swal.fire({
                    title: 'Gagal',
                    text: res.mssg,
                    icon: 'error'
                });
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            Swal.fire({
                title: 'Gagal',
                text: 'Terjadi kesalahan saat mengirim data',
                icon: 'error'
            });
        });
    });

    table_data = $('#table-teknisi').DataTable({
        // serverSide: true,
        processing: true,
        theme: 'bootstrap',
        ajax: {
            url: "/admin/data-teknisi/data",
            type: 'get',
            dataType: 'json'
        },
        columnDefs: [{
            targets: [0],
            searchable: false,
            orderable: false,
            className: 'text-center'
        },
        ],
        columns: [{
            data: 'DT_RowIndex'
        }, {
            data: 'nama_teknisi'
        }, {
            data: 'nomor_wa_teknisi',
            render: (data,type,row) => {
                const button_wa = $('<button>', {
                    class: 'btn btn-outline-success btn-wa',
                    html: '<i class="bx bxl-whatsapp"></i> ' + data,
                    'data-id': data,
                    title: 'Kirim Whatsapp',
                    'data-placement': 'top',
                    'data-toggle': 'tooltip'
                });

                return $('<div>', {
                    class: 'btn-group',
                    html: () => {
                        let arr = [];
                        arr.push(button_wa)
                        return arr;
                    }
                }).prop('outerHTML');
            }
        }, {
            data: 'id_teknisi',
            render: (data, type, row) => {
                const button_edit = $('<button>', {
                    class: 'btn btn-outline-primary btn-edit',
                    html: '<i class="bx bxs-edit"></i>',
                    'data-id': data,
                    title: 'Ubah Data',
                    'data-placement': 'top',
                    'data-toggle': 'tooltip'
                });

                const button_delete = $('<button>', {
                    class: 'btn btn-outline-danger btn-delete',
                    html: '<i class="bx bxs-trash"></i>',
                    'data-id': data,
                    title: 'Hapus Data',
                    'data-placement': 'top',
                    'data-toggle': 'tooltip'
                });

                return $('<div>', {
                    class: 'btn-group',
                    html: () => {
                        let arr = [];
                        arr.push(button_edit, button_delete)
                        return arr;
                    }
                }).prop('outerHTML');
            }
        }]
    });

    $('#table-teknisi').on('click', '.btn-edit', function () {
        let data_row = table_data.row($(this).closest('tr')).data();
        $('#id_teknisi').val(data_row.id_teknisi);
        $('#nama_teknisi').val(data_row.nama_teknisi);
        $('#nomor_wa_teknisi').val(data_row.nomor_wa_teknisi);
        $('#modal-teknisi').modal('show');
    });

    $('#table-teknisi').on('click', '.btn-delete', function () {
        let data = table_data.row($(this).closest('tr')).data();
        let token = document.head.querySelector('meta[name="csrf-token"]').content;
        Swal.fire({
            title: "Anda yakin menghapus data ini?",
            text: "Data yang sudah terhapus tidak bisa dikembalikan!",
            icon: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#34c38f",
            cancelButtonColor: "#f46a6a",
            confirmButtonText: "Ya, Hapus!",
            cancelButtonText: "Batal"
        }).then((t) => {
            if (t.isConfirmed) {
                $.ajax({
                    // url: "{{ route('superadmin.produk.delete') }}",
                    url: "/admin/delete-teknisi",
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        id_teknisi: data.id_teknisi,
                        _method: 'DELETE',
                        _token: token
                    },
                }).done((res) => {
                    if (res.status) {
                        Swal.fire({
                            title: 'Sukses',
                            text: res.pesan,
                            icon: 'success'
                        });
                        table_data.ajax.reload();
                    } else {
                        Swal.fire({
                            title: 'Oops..!',
                            text: res.pesan,
                            icon: 'error'
                        });
                    }
                })
            }
        })
    });

    $('#table-teknisi').on('click', '.btn-wa', function () {
        let data_row = table_data.row($(this).closest('tr')).data();
        $('#no_pesan').val(data_row.nomor_wa_teknisi);
        // $('#nomor_wa_teknisi').val(data_row.nomor_wa_teknisi);
        $('#modal-wa').modal('show');
    });

    $('.btn-kirim').click(function (e) {
        e.preventDefault();
        // $send = "https://wa.me/" . $nomor_wa . "?text=" . urlencode($pesan_teknisi);

        var no_wa = $('#no_pesan').val();
        var pesan = $('#pesan_teknisi').val();
        var url = 'https://wa.me/'+no_wa+'?text='+pesan;

        // Membuka tab baru dengan URL yang telah ditentukan
        window.open(url, '_blank');
    });
});
