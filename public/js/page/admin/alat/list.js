$(document).ready(function () {
    $('#form-alat').on('submit', function (e) {
        e.preventDefault();

        var formData = new FormData(this);

        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
        }).done(function (response) {
            if (response.status) {
                iziToast.success({
                    title: 'OK',
                    icon: 'icon-success',
                    position: 'topCenter',
                    message: 'Data Berhasil Disimpan',
                });
                table_data.ajax.reload();
                $('#form-alat')[0].reset();
            } else {
                Swal.fire({
                    title: 'Gagal',
                    text: res.mssg,
                    icon: 'error'
                });
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            Swal.fire({
                title: 'Gagal',
                text: 'Terjadi kesalahan saat mengirim data',
                icon: 'error'
            });
        });
    });

    table_data = $('#table-alat').DataTable({
        // serverSide: true,
        processing: true,
        theme: 'bootstrap',
        ajax: {
            url: "/admin/data-alat/data",
            type: 'get',
            dataType: 'json'
        },
        columnDefs: [{
            targets: [0],
            searchable: false,
            orderable: false,
            className: 'text-center'
        },
        ],
        columns: [{
            data: 'DT_RowIndex'
        }, {
            data: 'nama_alat'
        }, {
            data: 'nomor_seri_alat'
        }, {
            data: 'teknisi[0].nama_teknisi'
        }, {
            data: 'id_alat',
            render: (data, type, row) => {
                const button_edit = $('<button>', {
                    class: 'btn btn-outline-primary btn-edit',
                    html: '<i class="bx bxs-edit"></i>',
                    'data-id': data,
                    title: 'Ubah Data',
                    'data-placement': 'top',
                    'data-toggle': 'tooltip'
                });

                const button_delete = $('<button>', {
                    class: 'btn btn-outline-danger btn-delete',
                    html: '<i class="bx bxs-trash"></i>',
                    'data-id': data,
                    title: 'Hapus Data',
                    'data-placement': 'top',
                    'data-toggle': 'tooltip'
                });

                return $('<div>', {
                    class: 'btn-group',
                    html: () => {
                        let arr = [];
                        arr.push(button_edit, button_delete)
                        return arr;
                    }
                }).prop('outerHTML');
            }
        }]
    });

    $('#table-alat').on('click', '.btn-edit', function () {
        // let data_row = table.row($(this).closest('tr')).data();
        let data_row = table_data.row($(this).closest('tr')).data();
        $('#id_alat').val(data_row.id_alat);
        $('#nama_alat').val(data_row.nama_alat);
        $('#nomor_seri_alat').val(data_row.nomor_seri_alat);
        $('#id_teknisi').val(data_row.id_teknisi);
        $('#modal-alat').modal('show');
    });

    $('#table-alat').on('click', '.btn-delete', function () {
        let data = table_data.row($(this).closest('tr')).data();
        let token = document.head.querySelector('meta[name="csrf-token"]').content;
        Swal.fire({
            title: "Anda yakin menghapus data ini?",
            text: "Data yang sudah terhapus tidak bisa dikembalikan!",
            icon: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#34c38f",
            cancelButtonColor: "#f46a6a",
            confirmButtonText: "Ya, Hapus!",
            cancelButtonText: "Batal"
        }).then((t) => {
            if (t.isConfirmed) {
                $.ajax({
                    // url: "{{ route('superadmin.produk.delete') }}",
                    url: "/admin/delete-alat",
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        id_alat: data.id_alat,
                        _method: 'DELETE',
                        _token: token
                    },
                }).done((res) => {
                    if (res.status) {
                        Swal.fire({
                            title: 'Sukses',
                            text: res.pesan,
                            icon: 'success'
                        });
                        table_data.ajax.reload();
                    } else {
                        Swal.fire({
                            title: 'Oops..!',
                            text: res.pesan,
                            icon: 'error'
                        });
                    }
                })
            }
        })
    });
});
