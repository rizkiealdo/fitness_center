function doCountdowns() {
    $('.starttime').each(function (index) {
        doCountdown(this); // a td node
    });
}

function doCountdown(node) {
    let starttime = Date.parse($(node).html()) / 1000;
    let now = (Date.parse(new Date()) / 1000);
    let timeLeft = now - starttime;
    let minutes = Math.floor(timeLeft / 60);
    $(node).next("td").find(".durasi-input").val(minutes);
}



$(document).ready(function () {

    load_transaksi();

    function load_transaksi(){
        $.ajax({
            url: "/admin/data-transaksi",
            type: 'GET',
            success: function (response) {
                if (response) {
                    $('.transaksi-space').html(response);
                    checkout();
                    $('#table-transaksi').DataTable({
                        initComplete: function () {
                            setInterval(function () {
                                doCountdowns();
                            }, 1000);
                        }
                    });

                } else {
                    console.log('Data tidak ditemukan');
                }
            },
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    $('#btn-tambah').click(function (e) {
        e.preventDefault();
        $('#modal-checkin').modal('show')
    });

    $('#form-checkin').on('submit', function (e) {
        e.preventDefault(); // mencegah form untuk me-reload halaman
        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: $(this).serialize(),
            dataType: 'json',

        }).done((res) => {
            if (res.status) {
                iziToast.success({
                    title: 'OK',
                    icon: 'icon-success',
                    position: 'topCenter',
                    message: 'Data Berhasil Disimpan',
                });
                this.reset();
            } else {
                Swal.fire({
                    title: 'Gagal',
                    text: res.mssg,
                    icon: 'error'
                });
            }
        });
    });

    $(".id_customer").select2({
        dropdownParent: $('#modal-checkin'),
        width: 'resolve',
        theme: "bootstrap"
    });

    function checkNoMember() {
        $('#no_member').change(function (e) {
            e.preventDefault();
            let token = document.head.querySelector('meta[name="csrf-token"]').content;

            var noMember = $(this).val();
            $.ajax({
                url: "/admin/find-customer",
                type: 'POST',
                dataType: 'JSON',
                data: {
                    no_member: noMember,
                    _method: 'POST',
                    _token: token
                },
                success: function (response) {
                    if (response) {
                        console.log(response.id_customer);

                        // Menghapus opsi terpilih sebelumnya
                        $('.id_customer').find('option:selected').removeAttr('selected');

                        // Memilih opsi yang sesuai dengan respons
                        $('.id_customer').val(response.id_customer).change();
                    } else {
                        console.log('Data tidak ditemukan');
                    }
                },
            })
        });
    }


    $('.btn-pilih-paket').click(function (e) {
        $('#loading-form').show();
        $('#content-form').hide();
        e.preventDefault();

        let token = document.head.querySelector('meta[name="csrf-token"]').content;

        var idPaket = $(this).data('jenis');

        $('#modal-checkin').modal('hide');
        $('#modal-pilih-form').modal('show');
        $.ajax({
            url: "/admin/pilih-paket",
            type: 'POST',
            dataType: 'HTML',
            data: {
                id_paket: idPaket,
                _token: token
            },
            success: function (response) {
                if (response) {
                    $('#loading-form').hide();
                    $('#content-form').show();
                    $('#content-form').html(response);
                    checkNoMember();
                    checkin();
                } else {
                    console.log('Data tidak ditemukan');
                }
            },
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    });

    function checkin() {
        $('#form-checkin').on('submit', function (e) {
            e.preventDefault();

            var formData = new FormData(this);

            $.ajax({
                url: $(this).attr('action'),
                method: $(this).attr('method'),
                data: formData,
                dataType: 'json',
                contentType: false,
                processData: false,
            }).done(function (response) {
                if (response.status) {
                    iziToast.success({
                        title: 'OK',
                        icon: 'icon-success',
                        position: 'topCenter',
                        message: 'Data Berhasil Disimpan',
                    });
                    load_transaksi();
                    $('#form-checkin')[0].reset();
                } else if (response.status == false) {
                    Swal.fire({
                        title: 'Gagal',
                        text: response.msg,
                        icon: 'error'
                    });
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                Swal.fire({
                    title: 'Gagal',
                    text: 'Terjadi kesalahan saat mengirim data',
                    icon: 'error'
                });
            });
        });
    }

    function checkout(){
        $('.btn-checkout').click(function (e) {
            e.preventDefault();
            var id_transaksi = $(this).data('id');
            var durasi = $('#durasi_'+id_transaksi).val();
            let token = document.head.querySelector('meta[name="csrf-token"]').content;
            Swal.fire({
                title: "Anda yakin customer tersebut sudah checkout?",
                icon: "warning",
                showCancelButton: !0,
                confirmButtonColor: "#34c38f",
                cancelButtonColor: "#f46a6a",
                confirmButtonText: "Ya, Yakin!",
                cancelButtonText: "Batal"
            }).then((t) => {
                if (t.isConfirmed) {
                    $.ajax({
                        url: "/admin/checkout",
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            id_transaksi: id_transaksi,
                            durasi: durasi,
                            _method: 'POST',
                            _token: token
                        },
                    }).done((res) => {
                        if (res.status) {
                            Swal.fire({
                                title: 'Sukses',
                                text: res.pesan,
                                icon: 'success'
                            });
                            load_transaksi();
                        } else {
                            Swal.fire({
                                title: 'Oops..!',
                                text: res.pesan,
                                icon: 'error'
                            });
                        }
                    })
                }
            });
        });
    }

    function print_nota()
    {

    }

    $('#table-transaksi').on('click', '.btn-checkout', function () {
        let data = table_data.row($(this).closest('tr')).data();
        let durasi = $('#countup_'+data.id_transaksi).text();
        let token = document.head.querySelector('meta[name="csrf-token"]').content;
        Swal.fire({
            title: "Anda yakin customer tersebut sudah checkout?",
            icon: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#34c38f",
            cancelButtonColor: "#f46a6a",
            confirmButtonText: "Ya, Yakin!",
            cancelButtonText: "Batal"
        }).then((t) => {
            if (t.isConfirmed) {
                $.ajax({
                    url: "/admin/checkout",
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        id_transaksi: data.id_transaksi,
                        durasi: durasi,
                        _method: 'POST',
                        _token: token
                    },
                }).done((res) => {
                    if (res.status) {
                        Swal.fire({
                            title: 'Sukses',
                            text: res.pesan,
                            icon: 'success'
                        });
                        table_data.ajax.reload();
                    } else {
                        Swal.fire({
                            title: 'Oops..!',
                            text: res.pesan,
                            icon: 'error'
                        });
                    }
                })
            }
        })

    });

});
