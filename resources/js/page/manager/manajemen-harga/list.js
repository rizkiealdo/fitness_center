$(document).ready(function () {
    $('#btn-tambah').click(function (e) {
        e.preventDefault();
        $('#modal-paket').modal('show');
    });

    table_data = $('.table-harga').DataTable({
        serverSide: true,
        processing: true,
        theme: 'bootstrap',
        ajax: {
            url: "/manager/data-harga",
            type: 'get',
            dataType: 'json'
        },
        order: [4, 'desc'],
        columnDefs: [{
            targets: [0],
            searchable: false,
            orderable: false,
            className: 'text-center'
        },
        ],
        columns: [{
            data: 'DT_RowIndex'
        }, {
            data: 'jenis_paket'
        },
        {
            data: 'keterangan_paket'
        }, {
            data: 'harga_paket'
        }, {
            data: 'id_paket',
            render: (data, type, row) => {
                const button_edit = $('<button>', {
                    class: 'btn btn-outline-primary btn-edit',
                    html: '<i class="bx bxs-edit"></i>',
                    'data-id': data,
                    title: 'Ubah Data',
                    'data-placement': 'top',
                    'data-toggle': 'tooltip'
                });

                const button_delete = $('<button>', {
                    class: 'btn btn-outline-danger btn-delete',
                    html: '<i class="bx bxs-trash"></i>',
                    'data-id': data,
                    title: 'Hapus Data',
                    'data-placement': 'top',
                    'data-toggle': 'tooltip'
                });

                return $('<div>', {
                    class: 'btn-group',
                    html: () => {
                        let arr = [];
                        arr.push(button_edit, button_delete)
                        return arr;
                    }
                }).prop('outerHTML');
            }
        }]
    });

    $('.table-harga').on('click', '.btn-edit', function () {
        // let data_row = table.row($(this).closest('tr')).data();
        let data_row = table_data.row($(this).closest('tr')).data();
        $('#id_paket').val(data_row.id_paket);
        $('#jenis_paket').val(data_row.jenis_paket);
        $('#keterangan_paket').val(data_row.keterangan_paket);
        $('#harga_paket').val(data_row.harga_paket);
        $('#modal-paket').modal('show');
    });

 $('#form-paket').on('submit', function (e) {
        e.preventDefault();

        var formData = new FormData(this);

        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
        }).done(function (response) {
            if (response.status) {
                iziToast.success({
                    title: 'OK',
                    icon: 'icon-success',
                    position: 'topCenter',
                    message: 'Data Berhasil Disimpan',
                });
                table_data.ajax.reload();
                $('#form-paket')[0].reset();
            } else {
                Swal.fire({
                    title: 'Gagal',
                    text: res.mssg,
                    icon: 'error'
                });
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            Swal.fire({
                title: 'Gagal',
                text: 'Terjadi kesalahan saat mengirim data',
                icon: 'error'
            });
        });
    });


    $('.table-harga').on('click', '.btn-delete', function () {
        let data = table_data.row($(this).closest('tr')).data();
        let {
            id_paket
        } = data;
        let token = document.head.querySelector('meta[name="csrf-token"]').content;
        Swal.fire({
            title: "Anda yakin menghapus data ini?",
            text: "Data yang sudah terhapus tidak bisa dikembalikan!",
            icon: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#34c38f",
            cancelButtonColor: "#f46a6a",
            confirmButtonText: "Ya, Hapus!",
            cancelButtonText: "Batal"
        }).then((t) => {
            if (t.isConfirmed) {
                $.ajax({
                    url: '{{ route("manager.hapus-harga") }}',
                    // url: "/manager/hapus-harga",
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        id_paket: data.id_paket,
                        _method: 'DELETE',
                        _token: token
                    },
                }).done((res) => {
                    if (res.stats) {
                        Swal.fire({
                            title: 'Sukses',
                            text: res.mssg,
                            icon: 'success'
                        });
                        table_data.ajax.reload();
                    } else {
                        Swal.fire({
                            title: 'Oops..!',
                            text: res.mssg,
                            icon: 'error'
                        });
                    }
                })
            }
        })
    });



});
