                        <div class='container mb-3'>
                            <!-- Card 1 -->
                            <div class='card'>
                                <div class='card__info'
                                    style="background-image: url('{{asset('img/squat_1-min-min_compressed-1.webp')}}')">
                                    <h2 class='card__name'>BASIC</h2>
                                    <p class='card__price' style='color: var(--color05)'>$19.99 <span
                                            class='card__priceSpan'>/month</span></p>
                                </div>
                                <div class='card__content' style='border-color: var(--color05)'>
                                    <div class='card__rows'>
                                        <p class='card__row'>5GB Disk Space</p>
                                        <p class='card__row'>10 Domain Names</p>
                                        <p class='card__row'>5 E-Mail Address</p>
                                        <p class='card__row'>50GB Monthly Bandwidth</p>
                                        <p class='card__row'>Fully Support</p>
                                    </div>
                                    <a href='#emptyLink' class='card__link'
                                        style='background-color: var(--color02)'>PURCHASE</a>
                                </div>
                            </div>

                            <!-- Card 2 -->
                            <div class='card'>
                                <div class='card__info'
                                    style="background-image: url('{{asset('img/BigAndBulky-Article-FeaturedImage.jpg')}}')">
                                    <h2 class='card__name'>STANDARD</h2>
                                    <p class='card__price' style='color: var(--color06)'>$29.99 <span
                                            class='card__priceSpan'>/month</span></p>
                                </div>
                                <div class='card__content' style='border-color: var(--color06)'>
                                    <div class='card__rows'>
                                        <p class='card__row'>10GB Disk Space</p>
                                        <p class='card__row'>20 Domain Names</p>
                                        <p class='card__row'>10 E-Mail Address</p>
                                        <p class='card__row'>100GB Monthly Bandwidth</p>
                                        <p class='card__row'>Fully Support</p>
                                    </div>
                                    <a href='#emptyLink' class='card__link'
                                        style='background-color: var(--color06)'>PURCHASE</a>
                                </div>
                            </div>

                            <!-- Card 3 -->
                            <div class='card'>
                                <div class='card__info'
                                    style="background-image: url('{{asset('img/personal-training_intro.png')}}')">
                                    <h2 class='card__name'>PREMIUM</h2>
                                    <p class='card__price' style='color: var(--color12)'>$49.99 <span
                                            class='card__priceSpan'>/month</span></p>
                                </div>
                                <div class='card__content' style='border-color: var(--color07)'>
                                    <div class='card__rows'>
                                        <p class='card__row'>50GB Disk Space</p>
                                        <p class='card__row'>50 Domain Names</p>
                                        <p class='card__row'>20 E-Mail Address</p>
                                        <p class='card__row'>300GB Monthly Bandwidth</p>
                                        <p class='card__row'>Fully Support</p>
                                    </div>
                                    <a href='#emptyLink' class='card__link'
                                        style='background-color: var(--color04)'>PURCHASE</a>
                                </div>
                            </div>
                        </div>
