@extends('layouts.app')
@section('title', 'Manajemen Harga | Manager')
@push('style')
{{-- <link rel="stylesheet" href="{{asset('css/page/manager/manajemen-harga/list.css')}}"> --}}
@endpush
@section('content')
<div class="main_content_iner ">
    <div class="container-fluid p-0 sm_padding_15px">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="dashboard_header mb_50">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="dashboard_header_title">
                                <h3> Manajemen Harga</h3>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="dashboard_breadcam text-end">
                                <button type="button" class="btn btn-primary" id="btn-tambah">Tambah Data</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="card">
                        <div class="card-body">

                            <div class="table-responsive">
                                <table class="table table-harga" id="table-harga">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Jenis Paket</th>
                                            <th>Keterangan Paket</th>
                                            <th>Harga Paket</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <hr>

        <div class="row justify-content-center mt-3 mb_30">
            {{-- Manajemen Discount Start Here --}}
            <div class="col-12">
                <div class="dashboard_header mb_30">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="dashboard_header_title">
                                <h3> Manajemen Discount</h3>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="dashboard_breadcam text-end">
                                <button type="button" class="btn btn-primary" id="btn-tambah-discount">Tambah Data Discount</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="card">
                        <div class="card-body">

                            <div class="table-responsive">
                                <table class="table table-discount" id="table-discount">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Jenis Discount</th>
                                            <th>Discount (%)</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            {{-- Manajemen Discount Start Here --}}
        </div>
    </div>
</div>

{{-- Modal Tambah Data --}}
<div class="modal fade" id="modal-paket" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Manajemen Harga Paket</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="form-paket" method="POST" action="{{ route('manager.simpan-harga') }}">
                    @csrf
                    <input type="hidden" name="id_paket" id="id_paket">

                    <label for="jenis_paket">Jenis Paket</label>
                    <select name="jenis_paket" id="jenis_paket" class="form-control">
                        <option value="" selected disabled>-- Pilih Jenis Paket --</option>
                        <option value="Harian">Harian</option>
                        <option value="Bulanan">Bulanan</option>
                        <option value="Bulanan + Trainer">Bulanan + Trainer</option>
                    </select>

                    <label for="fasilitas_paket">Fasilitas</label>
                    <textarea name="keterangan_paket" id="keterangan_paket" class="form-control"></textarea>

                    <label for="harga_paket">Harga</label>
                    <input type="number" name="harga_paket" id="harga_paket" class="form-control">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
{{-- Modal Tambah Data --}}

{{-- Modal Tambah Data Discount --}}
<div class="modal fade" id="modal-discount" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Manajemen Discount</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="form-discount" method="POST" action="{{ route('manager.simpan-discount') }}">
                    @csrf
                    <input type="hidden" name="id_discount" id="id_discount">

                    <label for="jenis_discount">Jenis Discount</label>
                    <input type="text" name="jenis_discount" id="jenis_discount" class="form-control">

                    <label for="discount">Discount <small class="text-danger">%</small></label>
                    <input type="number" name="discount" id="discount" class="form-control">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
{{-- Modal Tambah Data Discount --}}
@endsection

@push('script')
<script src="{{ asset('js/page/manager/manajemen-harga/list.js') }}"></script>
@endpush
