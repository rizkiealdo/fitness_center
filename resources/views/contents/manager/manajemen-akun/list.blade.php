@extends('layouts.app')
@section('title', 'Manajemen Akun | Manager')
@push('style')
<style>
/* Gaya tombol pagination */


</style>
@endpush
@section('content')
<div class="main_content_iner ">
    <div class="container-fluid p-0 sm_padding_15px">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="dashboard_header mb_50">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="dashboard_header_title">
                                <h3> Manajemen Akun</h3>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="container overflow-hidden bg-white">
                        <div class="card">
                            <div class="card-body">
                                <button type="button" class="btn btn-primary mb-3" id="btn-tambah-akun">Tambah Akun</button>
                                <div class="table-responsive QA_table mb_30 text-center">
                                    <table id="table-akun" class="table">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama</th>
                                                <th>Email</th>
                                                <th>Role</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

{{-- Modal Edit Akun --}}
<div class="modal fade" id="modal-plotting" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Modal title</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="form-plotting" method="POST" action="{{route('manager.simpan-akun')}}">
                    @csrf
                    <input type="hidden" name="id_user" id="id_user">
                    <label for="role_id">Plot Role Sebagai</label>
                    <select name="role_id" id="role_id" class="form-control">
                        <option value="" selected disabled>-- Pilih Role --</option>
                        <option value="1">Admin</option>
                        <option value="2">Manager</option>
                    </select>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
{{-- Modal Edit Akun --}}

{{-- Modal Tambah Akun --}}
<div class="modal fade" id="modal-tambah_akun" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Modal title</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="form-tambah-akun" method="POST" action="{{route('manager.tambah-akun')}}">
                    @csrf
                    <div class="mb-3">
                    <label for="nama">Nama</label>
                    <input type="text" name="name" id="name" class="form-control" placeholder="Nama Akun">
                    </div>
                    <div class="mb-3">
                    <label for="nama">Email</label>
                    <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
{{-- Modal Edit Akun --}}
@endsection

@push('script')
<script src="{{asset('js/page/manager/manajemen-akun/list.js')}}"></script>
@endpush
