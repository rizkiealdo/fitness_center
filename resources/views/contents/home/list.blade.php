@extends('layouts.home.app')
@section('content')
<!-- Hero -->
<div class="position-relative bg-primary overflow-hidden">
    <div class="container position-relative zi-2 content-space-3 content-space-md-5">
        <div class="w-md-75 w-xl-65 text-center mx-md-auto">
            <!-- Heading -->
            <div class="w-100 mb-7 text-center">
                <h1 class="display-6 fw-normal text-white lh-base">Sertifikasi dan Standardisasi<br>
                    <span class="text-accent">
                        <span class="js-typedjs" data-hs-typed-options='{
               "strings": ["Usaha Pariwisata"],
               "typeSpeed": 90,
               "loop": true,
               "backSpeed": 30,
               "backDelay": 2500
               }'></span>
                    </span>
                </h1>
            </div>
            <!-- End Heading -->

            <form id="search-form" action="https://sisupar.kemenparekraf.go.id/hasil-pencarian">
                <!-- Input Card -->
                <div class="input-card">
                    <div class="input-card-form">
                        <label for="searchLocationForm" class="form-label visually-hidden">Pencarian</label>
                        <div class="input-group input-group-merge">
                            <span class="input-group-prepend input-group-text">
                                <i class="bi-search"></i>
                            </span>
                            <input type="text" name="keyword" required class="form-control form-control-lg"
                                id="searchLocationForm" placeholder="Pencarian Usaha Tersertifikasi..."
                                aria-label="Pencarian Usaha Tersertifikasi...">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-lg">Cari</button>
                </div>
                <!-- End Input Card -->
            </form>
            <p class="form-text small text-center text-white-70">Ketik Usaha Tersertifikasi di Indonesia</p>
        </div>
    </div>

    <!-- Background Shape -->
    <figure class="position-absolute top-0 start-0 w-65">
        <svg width="1246" height="1078" viewBox="0 0 1246 1078" fill="none" xmlns="http://www.w3.org/2000/svg">
            <g clip-path="url(#clip0)">
                <path d="M519.8 0.599609H0V1078H863.4L519.8 0.599609Z" fill="url(#paint0_linear)" />
                <path d="M519.7 0L1039.4 0.6L1246 639.1L725.2 644L519.7 0Z" fill="url(#paint1_linear)" />
                <path
                    d="M50.7999 0.000390626L30.0999 57.4004C30.0999 57.4004 -0.600079 133.5 74.1999 175.6C74.1999 175.6 131.6 205.7 181.7 157.6C181.7 157.6 188.4 150.9 193.7 142.2C199 133.5 198.4 134.2 201.7 126.8C205 119.4 238.4 20.6004 238.4 20.6004C238.4 20.6004 241.1 13.9004 243.1 -0.0996094H50.7999V0.000390626Z"
                    fill="url(#paint2_linear)" />
                <path
                    d="M0 0C0 0 0 194.9 0 195C0 195.8 4.6 198.2 5.2 198.6C9.9 201.5 14.9 204 20.1 206C36.7 212.4 54.9 213.9 72.2 210C87.8 206.5 102.8 199.4 115.1 189.2C128.2 178.3 135.9 163.8 141.8 148.1C148.8 129.6 155.4 111 161.7 92.3C167.6 75 174.2 57.8 178.8 40C180.4 33.5 181.7 26.9 182.1 20.2C182.2 18.3 182.6 0 179.7 0H0Z"
                    fill="url(#paint3_linear)" />
            </g>
            <defs>
                <linearGradient id="paint0_linear" x1="526.492" y1="1.7247" x2="326.563" y2="1135.58"
                    gradientUnits="userSpaceOnUse">
                    <stop offset="0.4976" stop-color="#145388" />
                    <stop offset="1" stop-color="#132B51" />
                </linearGradient>
                <linearGradient id="paint1_linear" x1="882.835" y1="3.77956e-05" x2="882.835" y2="644.042"
                    gradientUnits="userSpaceOnUse">
                    <stop offset="1.57705e-06" stop-color="#145388" />
                    <stop offset="1" stop-color="#132B51" />
                </linearGradient>
                <linearGradient id="paint2_linear" x1="126.049" y1="-9.37791" x2="149.819" y2="181.458"
                    gradientUnits="userSpaceOnUse">
                    <stop stop-color="#145388" />
                    <stop offset="1" stop-color="#132B51" />
                </linearGradient>
                <linearGradient id="paint3_linear" x1="190.821" y1="18.3911" x2="-28.7839" y2="145.18"
                    gradientUnits="userSpaceOnUse">
                    <stop offset="1.95387e-07" stop-color="#145388" />
                    <stop offset="1" stop-color="#093A66" />
                </linearGradient>
                <clipPath id="clip0">
                    <rect width="1246" height="1078" fill="white" />
                </clipPath>
            </defs>
        </svg>

    </figure>
    <!-- End Background Shape -->

    <!-- Shape -->
    <div class="shape shape-bottom zi-1" style="margin-bottom: -.125rem">
        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 1920 100.1">
            <path fill="#fff" d="M0,0c0,0,934.4,93.4,1920,0v100.1H0L0,0z"></path>
        </svg>
    </div>
    <!-- End Shape -->
</div>
<!-- End Hero -->

<!-- Swiper -->
<div class="js-swiper-slides swiper-container content-space-1 content-space-sm-2">
    <div class="swiper-wrapper">
        <div class="swiper-slide">
            <a href="detail-standardisasi/bar.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/610/76c/cb0/61076ccb0d763565290844.png')}}" alt="Bar">
                <span class="text-center text-primary fs-6">Bar</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/diskotek.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/610/76c/df1/61076cdf1ac60990838239.png')}}" alt="Diskotek">
                <span class="text-center text-primary fs-6">Diskotek</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/kelab-malam-atau-diskotik-yang-utamanya-menyediakan-minuman.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/610/76d/146/61076d146b549403140019.png')}}"
                    alt="Kelab Malam Atau Diskotik Yang Utamanya Menyediakan Minuman">
                <span class="text-center text-primary fs-6">Kelab Malam Atau Diskotik Yang Utamanya Menyediakan
                    Minuman</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/sante-par-aqua-spa-tirta-1.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/610/76d/4cd/61076d4cdbb0d453971424.png')}}"
                    alt="Sante Par Aqua (Spa) Tirta 1">
                <span class="text-center text-primary fs-6">Sante Par Aqua (Spa) Tirta 1</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/vila-bintang-3-diamond.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/610/76d/598/61076d598cc75482573559.png')}}"
                    alt="Vila Bintang 3 (Diamond)">
                <span class="text-center text-primary fs-6">Vila Bintang 3 (Diamond)</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/wisata-arung-jeram.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/610/76d/66a/61076d66ae1ca593638713.png')}}" alt="Wisata Arung Jeram">
                <span class="text-center text-primary fs-6">Wisata Arung Jeram</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/wisata-memancing.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/610/769/a23/610769a23a6f1664123341.png')}}" alt="Wisata Memancing">
                <span class="text-center text-primary fs-6">Wisata Memancing</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/wisata-selam.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/610/76d/733/61076d733202b445970224.png')}}" alt="Wisata Selam">
                <span class="text-center text-primary fs-6">Wisata Selam</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/fasilitasi-gelanggangarena-biliar.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/610/76b/e67/61076be67e22a875882354.png')}}"
                    alt="Fasilitasi Gelanggang/Arena Biliar">
                <span class="text-center text-primary fs-6">Fasilitasi Gelanggang/Arena Biliar</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/fasilitasi-gelanggangarena-bowling.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/610/76b/f61/61076bf611967063129547.png')}}"
                    alt="Fasilitasi Gelanggang/Arena Bowling">
                <span class="text-center text-primary fs-6">Fasilitasi Gelanggang/Arena Bowling</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/fasilitasi-gelanggangarena-renang.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/610/76c/037/61076c037ba93086764746.png')}}"
                    alt="Fasilitasi Gelanggang/Arena Renang">
                <span class="text-center text-primary fs-6">Fasilitasi Gelanggang/Arena Renang</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/arena-permainan.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/610/76a/19b/61076a19b32a9842767362.png')}}" alt="Arena Permainan">
                <span class="text-center text-primary fs-6">Arena Permainan</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/pengelolaan-goa.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/610/76d/8f8/61076d8f8ee2e899343004.png')}}" alt="Pengelolaan Goa">
                <span class="text-center text-primary fs-6">Pengelolaan Goa</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/angkutan-laut-dalam-negeri-untuk-wisata-berakomodasi.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/610/76d/c5c/61076dc5c18bb147050462.png')}}"
                    alt="Angkutan Laut Dalam Negeri Untuk Wisata (Berakomodasi)">
                <span class="text-center text-primary fs-6">Angkutan Laut Dalam Negeri Untuk Wisata
                    (Berakomodasi)</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/angkutan-laut-luar-negeri-untuk-wisata.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/610/76d/d95/61076dd9545d7665494274.png')}}"
                    alt="Angkutan Laut Luar Negeri Untuk Wisata">
                <span class="text-center text-primary fs-6">Angkutan Laut Luar Negeri Untuk Wisata</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/angkutan-wisata-di-sungai-dan-danau-untuk-wisata-dan-yang-berhubungan-dengan-itu.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/610/76d/f2c/61076df2c6e92008414512.png')}}"
                    alt="Angkutan Wisata di Sungai Dan Danau Untuk Wisata Dan Yang Berhubungan Dengan Itu">
                <span class="text-center text-primary fs-6">Angkutan Wisata di Sungai Dan Danau Untuk Wisata Dan Yang
                    Berhubungan Dengan Itu</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/wisata-petualangan-alam.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/610/76e/33b/61076e33be17b565165449.png')}}"
                    alt="Wisata Petualangan Alam">
                <span class="text-center text-primary fs-6">Wisata Petualangan Alam</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/aktivitas-wisata-air.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/637/c31/bb9/637c31bb9f0c1807406379.png')}}" alt="Aktivitas Wisata Air">
                <span class="text-center text-primary fs-6">Aktivitas Wisata Air</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/angkutan-jalan-rel-wisata.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/635/3d6/808/6353d6808bed8546361423.png')}}"
                    alt="Angkutan Jalan Rel Wisata">
                <span class="text-center text-primary fs-6">Angkutan Jalan Rel Wisata</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/fasilitasi-gelanggang-arena-bungee-jumping.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/637/c30/83a/637c3083a6090915076179.png')}}"
                    alt="Fasilitasi Gelanggang /Arena Bungee Jumping">
                <span class="text-center text-primary fs-6">Fasilitasi Gelanggang /Arena Bungee Jumping</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/jasa-boga-untuk-suatu-event-tertentu-event-catering.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/635/3be/eb2/6353beeb21788139609563.png')}}"
                    alt="Jasa Boga Untuk Suatu Event Tertentu (Event Catering)">
                <span class="text-center text-primary fs-6">Jasa Boga Untuk Suatu Event Tertentu (Event Catering)</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/penyediaan-jasa-boga-periode-tertentu.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/635/3c0/0dd/6353c00dd96c3031851903.png')}}"
                    alt="Penyediaan Jasa Boga Periode Tertentu">
                <span class="text-center text-primary fs-6">Penyediaan Jasa Boga Periode Tertentu</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/wisata-tirta-lainnya.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/610/863/7a4/6108637a42460000555499.png')}}" alt="Wisata Tirta Lainnya">
                <span class="text-center text-primary fs-6">Wisata Tirta Lainnya</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/sante-par-aqua-spa-tirta-2.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/635/39c/006/63539c006794e138897464.png')}}"
                    alt="Sante Par Aqua (Spa) Tirta 2">
                <span class="text-center text-primary fs-6">Sante Par Aqua (Spa) Tirta 2</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/sante-par-aqua-spa-tirta-3.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/637/c33/23e/637c3323e075d322920251.png')}}"
                    alt="Sante par Aqua (Spa) Tirta 3">
                <span class="text-center text-primary fs-6">Sante par Aqua (Spa) Tirta 3</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/vila-bintang-2-gold.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/635/3a5/438/6353a54380803615397866.png')}}"
                    alt="Vila Bintang 2 (Gold)">
                <span class="text-center text-primary fs-6">Vila Bintang 2 (Gold)</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/apartemen-hotel-berisiko-menengah-tinggi.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/635/3b2/61c/6353b261c3797169559791.png')}}"
                    alt="Apartemen Hotel Berisiko Menengah Tinggi">
                <span class="text-center text-primary fs-6">Apartemen Hotel Berisiko Menengah Tinggi</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/restoran-berisiko-menengah-tinggi.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/635/3bb/a28/6353bba28d00d402752669.png')}}"
                    alt="Restoran Berisiko Menengah Tinggi">
                <span class="text-center text-primary fs-6">Restoran Berisiko Menengah Tinggi</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/fasilitasi-gelanggangarena-paralayang-paragliding-dan-layang-gantung-hang-gliding.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/635/600/84d/63560084defb0829676761.png')}}"
                    alt="Fasilitasi Gelanggang/Arena Paralayang (Paragliding) Dan Layang Gantung (Hang Gliding)">
                <span class="text-center text-primary fs-6">Fasilitasi Gelanggang/Arena Paralayang (Paragliding) Dan
                    Layang Gantung (Hang Gliding)</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/fasilitasi-gelanggangarena-hoki-es.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/635/602/297/6356022971658278576332.png')}}"
                    alt="Fasilitasi Gelanggang/Arena Hoki Es">
                <span class="text-center text-primary fs-6">Fasilitasi Gelanggang/Arena Hoki Es</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/fasilitasi-pengelolaan-gelanggangarena-slingshot.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/635/603/24e/63560324e7748854607036.png')}}"
                    alt="Fasilitasi Pengelolaan Gelanggang/Arena Slingshot">
                <span class="text-center text-primary fs-6">Fasilitasi Pengelolaan Gelanggang/Arena Slingshot</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/angkutan-laut-dalam-negeri-untuk-wisata-tidak-berakomodasi.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/635/60c/868/63560c8685025029969296.png')}}"
                    alt="Angkutan Laut Dalam Negeri Untuk Wisata (Tidak Berakomodasi)">
                <span class="text-center text-primary fs-6">Angkutan Laut Dalam Negeri Untuk Wisata (Tidak
                    Berakomodasi)</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/klub-malam.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/637/c33/c4f/637c33c4f3824587321863.png')}}" alt="Klub Malam">
                <span class="text-center text-primary fs-6">Klub Malam</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/hotel-bintang-berisiko-menengah-tinggi.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/636/0b2/1ed/6360b21ed6ac7151243453.png')}}"
                    alt="Hotel Bintang Berisiko Menengah Tinggi">
                <span class="text-center text-primary fs-6">Hotel Bintang Berisiko Menengah Tinggi</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/hotel-melati-berisiko-menengah-tinggi.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/638/5d6/af0/6385d6af05088420969274.png')}}"
                    alt="Hotel Melati Berisiko Menengah Tinggi">
                <span class="text-center text-primary fs-6">Hotel Melati Berisiko Menengah Tinggi</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/kawasan-pariwisata.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/610/769/5cc/6107695cc8654364068975.png')}}" alt="Kawasan Pariwisata">
                <span class="text-center text-primary fs-6">Kawasan Pariwisata</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/lapangan-golf.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/610/769/712/610769712fa68004180214.png')}}" alt="Lapangan Golf">
                <span class="text-center text-primary fs-6">Lapangan Golf</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/restoran-berisiko-tinggi.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/610/76d/318/61076d318d491763681127.png')}}"
                    alt="Restoran Berisiko Tinggi">
                <span class="text-center text-primary fs-6">Restoran Berisiko Tinggi</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/taman-rekreasi.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/610/769/8a2/6107698a26287734015483.png')}}" alt="Taman Rekreasi">
                <span class="text-center text-primary fs-6">Taman Rekreasi</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/dermaga-marina.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/610/769/c6e/610769c6eefa9253283908.png')}}" alt="Dermaga Marina">
                <span class="text-center text-primary fs-6">Dermaga Marina</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/apartemen-hotel-berisiko-tinggi.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/635/3b2/d48/6353b2d480b71621722956.png')}}"
                    alt="Apartemen Hotel Berisiko Tinggi">
                <span class="text-center text-primary fs-6">Apartemen Hotel Berisiko Tinggi</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/hotel-bintang-berisiko-tinggi.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/636/0b2/e5d/6360b2e5d55f6382617101.png')}}"
                    alt="Hotel Bintang Berisiko Tinggi">
                <span class="text-center text-primary fs-6">Hotel Bintang Berisiko Tinggi</span>
            </a>
        </div>
        <div class="swiper-slide">
            <a href="detail-standardisasi/hotel-melati-berisiko-tinggi.html"
                class="d-flex flex-column align-items-center justify-content-center gap-3 card-transition">
                <img class="" style="height: 2rem;"
                    src="{{asset('uploads/public/638/5d7/3cc/6385d73cc1f60843920212.png')}}"
                    alt="Hotel Melati Berisiko Tinggi">
                <span class="text-center text-primary fs-6">Hotel Melati Berisiko Tinggi</span>
            </a>
        </div>
    </div>
</div>
<!-- End Swiper -->
<hr class="my-0">
<!-- Process Section -->
<div class="container content-space-2 content-space-md-3">
    <!-- Title -->
    <div class="w-md-80 w-lg-50 text-center mx-md-auto mb-9">
        <span class="badge rounded-pill text-success bg-soft-success mb-2 py-2 px-4 fw-semibold">Mengapa Sertifikasi
            Usaha</span>
        <h2 class="fs-1 fw-light text-primary">Standar Usaha Pariwisata</h2>
        <p class="text-dark">Karena sertifikasi adalah kunci agar usaha Anda menjadi lebih terpercaya.</p>
    </div>
    <!-- End Title -->
    <div class="row">
        <div class="col-md-4 mb-4">
            <div class="d-flex flex-column">
                <div class="position-relative mb-10 mt-10">
                    <div class="position-absolute top-100 start-50 translate-middle">
                        <img class=""
                            src="{{asset('themes/sisupar/assets/landing/svg/components/circle-process-1.svg')}}">
                    </div>
                    <div class="position-absolute top-100 start-50 translate-middle">
                        <img class="mt-8" style="height: 3rem;"
                            src="{{asset('themes/sisupar/assets/landing/img/icons/yelp.png')}}">
                    </div>
                </div>
                <div class="text-center mt-3">
                    <h3 class="fs-3 fw-light">Professionalitas</h3>
                    <p class="text-dark mb-md-0">Usaha Anda akan terlihat lebih profesional dengan memiliki
                        Sertifikasi.</p>
                </div>
            </div>
        </div>
        <div class="col-md-4 mb-4">
            <div class="d-flex flex-column">
                <div class="position-relative mb-10 mt-10">
                    <div class="position-absolute top-100 start-50 translate-middle">
                        <img class=""
                            src="{{asset('themes/sisupar/assets/landing/svg/components/circle-process-2.svg')}}">
                    </div>
                    <div class="position-absolute top-100 start-50 translate-middle">
                        <div class="position-absolute top-100 start-50 translate-middle">
                            <img class="mt-8" style="height: 3rem;"
                                src="{{('themes/sisupar/assets/landing/img/icons/fire.png')}}">
                        </div>
                    </div>
                </div>
                <div class="text-center mt-3">
                    <h3 class="fs-3 fw-light">Kepercayaan Pelanggan</h3>
                    <p class="text-dark mb-md-0">Pelanggan Anda akan lebih yakin dalam memutuskan pembelian.</p>
                </div>
            </div>
        </div>
        <div class="col-md-4 mb-4">
            <div class="d-flex flex-column">
                <div class="position-relative mb-10 mt-10">
                    <div class="position-absolute top-100 start-50 translate-middle">
                        <img class=""
                            src="{{asset('themes/sisupar/assets/landing/svg/components/circle-process-3.svg')}}">
                    </div>
                    <div class="position-absolute top-100 start-50 translate-middle">
                        <div class="position-absolute top-100 start-50 translate-middle">
                            <img class="mt-8" style="height: 3rem;"
                                src="{{asset('themes/sisupar/assets/landing/img/icons/whmcs-brands.png')}}">
                        </div>
                    </div>
                </div>
                <div class="text-center mt-3">
                    <h3 class="fs-3 fw-light">Legalitas</h3>
                    <p class="text-dark mb-md-0">Usaha Anda akan dilindungi dan difasilitasi oleh Pemerintah.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Process Section -->
<!-- Video Content Section -->
<div id="html5VideoPlayer" class="position-relative video-player video-player-inline-btn"
    style="height: 480px !important;">
    <img class="img-fluid video-player-preview" src="{{asset('themes/sisupar/assets/landing/video/SUP_Flyer.jpg')}}"
        alt="Video Promotion">
    <!-- Play Button -->
    <a class="js-inline-video-player video-player-btn video-player-centered" href="javascript:;"
        data-hs-video-player-options='{
   "videoType": "html5",
   "videoId": "https://sisupar.kemenparekraf.go.id/themes/sisupar/ assets/landing/video/SUP_HD",
    "parentSelector": "#html5VideoPlayer",
    "targetSelector": "#html5VideoIframe",
    "isAutoplay": true,
    "control": true
    }'>
        <span class="video-player-icon shadow-sm">
            <i class="bi-play-fill"></i>
        </span>
    </a>
    <!-- End Play Button -->
    <!-- Video Iframe -->
    <div class="ratio ratio-16x9" style="height: 480px !important;">
        <div id="html5VideoIframe"></div>
    </div>
    <!-- End Video Iframe -->
    <!-- SVG Background -->
    <figure class="position-absolute bottom-0 start-0 w-100">
        <img class="js-svg-injector" style="width: 100%; height: 140px;"
            src="{{asset('themes/sisupar/assets/landing/svg/components/wave-1-bottom-sm.svg')}}"
            data-parent="#SVGhireUsBg">
    </figure>
    <!-- End SVG Background Section -->
</div>
<!-- End Video Block -->
<!-- End Video Content -->
<!-- CTA Section -->
<div class="bg-primary content-space-2">
    <div class="text-center py-5"
        style="background: url('themes/sisupar/assets/landing/svg/components/bg-elements-10.svg') center no-repeat;">
        <h2 class="fs-2 fw-light text-white">Uji kelayakan usaha Anda pada Standar Usaha Pariwisata!</h2>
        <p class="text-white-70">Memulai usaha pariwisata dengan mengenali standar.<br>Berdasarkan basis
            resiko rendah, menengah rendah, menengah tinggi, dan resiko tinggi.</p>
        <span class="d-block mt-5">
            <a class="btn btn-sm btn-light btn-transition" href="#">Pelajari Lebih Lanjut</a>
        </span>
    </div>
</div>
<!-- End CTA Section -->


<!-- Mockup Block Section -->
<div class="container content-space-t-2 content-space-t-md-3 content-space-b-1">
    <!-- Title -->
    <div class="w-md-80 w-lg-50 text-center mx-md-auto mb-9">
        <span class="badge rounded-pill text-primary bg-soft-primary mb-2 py-2 px-4 fw-semibold">Akses Mudah</span>
        <h2 class="fs-1 fw-light text-primary">Fasilitas untuk usaha Anda</h2>
        <p class="text-dark">Beragam fitur menarik dalam satu platform untuk memudahkan usaha Anda..</p>
    </div>
    <!-- End Title -->

    <div class="row justify-content-lg-between align-items-center">
        <div class="col-md-6 mb-7">

            <!-- Step -->
            <ul class="step step-dashed mb-7">
                <li class="step-item">
                    <div class="step-content-wrapper">
                        <span class="step-icon step-icon-xs step-icon-soft-primary">A.</span>
                        <div class="step-content">
                            <h4 class="step-title">Direktori Usaha Tersertifikasi</h4>
                            <p>Usaha Pariwisata Anda yang telah tersertifikasi akan terpublikasikan pada halaman
                                resmi Portal Usaha Online</p>
                        </div>
                    </div>
                </li>

                <li class="step-item mb-0">
                    <div class="step-content-wrapper">
                        <span class="step-icon step-icon-xs step-icon-soft-primary">B.</span>
                        <div class="step-content">
                            <h4 class="step-title">Direktori LSU</h4>
                            <p class="mb-0">Mudahkan proses sertifikasi dengan memilih Lembaga Sertifikasi Usaha
                                Pariwisata yang terdaftar oleh Kementerian Pariwisata dan Ekonomi Kreatif.</p>
                        </div>
                    </div>
                </li>
            </ul>
            <!-- End Step -->

            <a class="btn btn-sm btn-primary rounded-pill btn-transition" href="daftar-lsup.html">Lembaga Sertifikasi <i
                    class="bi bi-chevron-right ms-2"></i></a>

        </div>

        <div class="col-md-6">
        </div>
    </div>
</div>
<!-- End Mockup Block Section -->

<!-- Mockup Block Section -->
<div class="container">
    <div class="row justify-content-lg-between align-items-center" data-aos="fade-up">
        <div class="col-md-6 col-lg-5 order-md-2 mb-7 mb-md-0">

            <!-- Step -->
            <ul class="step step-dashed mb-7">
                <li class="step-item">
                    <div class="step-content-wrapper">
                        <span class="step-icon step-icon-xs step-icon-soft-primary">C.</span>
                        <div class="step-content">
                            <h4 class="step-title">QR Code untuk pelanggan</h4>
                            <p> Berikan pelanggan Anda kesempatan untuk mengetahui sertifikat usaha Anda kapan saja,
                                dimana saja.</p>
                        </div>
                    </div>
                </li>

                <li class="step-item mb-0">
                    <div class="step-content-wrapper">
                        <span class="step-icon step-icon-xs step-icon-soft-primary">D.</span>
                        <div class="step-content">
                            <h4 class="step-title">Diferensiasi Usaha Anda</h4>
                            <p class="mb-0">Tingkatkan standar usaha Anda dengan mendapatkan Sertifikat resmi yang
                                diakui oleh Negara.</p>
                        </div>
                    </div>
                </li>
            </ul>
            <!-- End Step -->

            <a class="btn btn-sm btn-primary rounded-pill btn-transition"
                href="usaha-tersertifikasi/direktori.html">Direktori Usaha <i class="bi bi-chevron-right ms-2"></i></a>

        </div>

        <div class="col-md-6 d-none d-sm-block">
            <!-- SVG Element -->
            <div class="position-relative mx-auto" style="max-width: 28rem; min-height: 30rem;">
                <figure class="position-absolute top-0 end-0 zi-2 me-10" data-aos="fade-up">
                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 450 450" width="165"
                        height="165">
                        <g>
                            <defs>
                                <path id="circleImgID2" d="M225,448.7L225,448.7C101.4,448.7,1.3,348.5,1.3,225l0,0C1.2,101.4,101.4,1.3,225,1.3l0,0
                  c123.6,0,223.7,100.2,223.7,223.7l0,0C448.7,348.6,348.5,448.7,225,448.7z" />
                            </defs>
                            <clipPath id="circleImgID1">
                                <use xlink:href="#circleImgID2" />
                            </clipPath>
                            <g clip-path="url(#circleImgID1)">
                                <image width="450" height="450"
                                    xlink:href="{{asset('themes/sisupar/assets/landing/img/others/qr-1.jpg')}}"></image>
                            </g>
                        </g>
                    </svg>
                </figure>

                <figure class="position-absolute top-0 start-0" data-aos="fade-up" data-aos-delay="300">
                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 335.2 335.2" width="120"
                        height="120">
                        <circle fill="none" stroke="#377dff" stroke-width="75" cx="167.6" cy="167.6" r="130.1" />
                    </svg>
                </figure>

                <figure class="d-none d-sm-block position-absolute top-0 start-0 mt-10" data-aos="fade-up"
                    data-aos-delay="200">
                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 515 515" width="200"
                        height="200">
                        <g>
                            <defs>
                                <path id="circleImgID4" d="M260,515h-5C114.2,515,0,400.8,0,260v-5C0,114.2,114.2,0,255,0h5c140.8,0,255,114.2,255,255v5
                  C515,400.9,400.8,515,260,515z" />
                            </defs>
                            <clipPath id="circleImgID3">
                                <use xlink:href="#circleImgID4" />
                            </clipPath>
                            <g clip-path="url(#circleImgID3)">
                                <image width="515" height="515"
                                    xlink:href="{{asset('themes/sisupar/assets/landing/img/others/qr-2.jpg')}}"
                                    transform="matrix(1 0 0 1 1.639390e-02 2.880859e-02)"></image>
                            </g>
                        </g>
                    </svg>
                </figure>

                <figure class="position-absolute top-0 end-0" style="margin-top: 11rem; margin-right: 13rem;"
                    data-aos="fade-up" data-aos-delay="250">
                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 67 67" width="25" height="25">
                        <circle fill="#00C9A7" cx="33.5" cy="33.5" r="33.5" />
                    </svg>
                </figure>

                <figure class="position-absolute top-0 end-0 me-3" style="margin-top: 8rem;" data-aos="fade-up"
                    data-aos-delay="350">
                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 141 141" width="50"
                        height="50">
                        <circle fill="#FFC107" cx="70.5" cy="70.5" r="70.5" />
                    </svg>
                </figure>

                <figure class="position-absolute bottom-0 end-0" data-aos="fade-up" data-aos-delay="400">
                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 770.4 770.4" width="280"
                        height="280">
                        <g>
                            <defs>
                                <path id="circleImgID6" d="M385.2,770.4L385.2,770.4c212.7,0,385.2-172.5,385.2-385.2l0,0C770.4,172.5,597.9,0,385.2,0l0,0
                  C172.5,0,0,172.5,0,385.2l0,0C0,597.9,172.4,770.4,385.2,770.4z" />
                            </defs>
                            <clipPath id="circleImgID5">
                                <use xlink:href="#circleImgID6" />
                            </clipPath>
                            <g clip-path="url(#circleImgID5)">
                                <image width="900" height="900"
                                    xlink:href="{{asset('themes/sisupar/assets/landing/img/others/qr-3.jpg')}}"
                                    transform="matrix(1 0 0 1 -64.8123 -64.8055)"></image>
                            </g>
                        </g>
                    </svg>
                </figure>
            </div>
            <!-- End SVG Element -->
        </div>
    </div>
</div>
<!-- End Mockup Block Section -->
@endsection