@extends('layouts.app')
@section('title', 'Laporan')
@section('content')
<div class="main_content_iner ">
    <div class="container-fluid p-0 sm_padding_15px">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="dashboard_header mb_50">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="dashboard_header_title">
                                <h3>Laporan Bulanan</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="card" style="border: none;">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-laporan" id="table-laporan">
                                    <thead>
                                        <tr class="text-center">
                                            <th>No.</th>
                                            <th>Bulan</th>
                                            <th>Jumlah Transaksi</th>
                                            <th>Omset</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                        $no = 0;
                                        @endphp
                                        @forelse ($data as $item)
                                        @php
                                        $no++;
                                        @endphp
                                        <tr class="text-center">
                                            <td style="width: 5%">{{$no}}</td>
                                            <td>{{$item->month_year}}</td>
                                            <td>{{$item->jumlah_transaksi}}</td>
                                            <td>{{$item->omset}}</td>
                                            <td>
                                                <a href="/cetak?bulan_tahun={{ $item->month_year }}" target="_blank" class="btn btn-primary btn-cetak">
                                                    Cetak Laporan Bulan {{ $item->month_year }}
                                                </a>
                                            </td>
                                        </tr>
                                        @empty

                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@push('script')
<script src="{{asset('js/page/laporan/list.js')}}"></script>
@endpush