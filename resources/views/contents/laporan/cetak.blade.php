<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laporan Bulanan</title>
    <style>
        .page_break {
            page-break-before: always;
        }
        table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
        }
    </style>
</head>

<body>
    <div class="container">
        <center>
            <h4>Laporan Bulanan</h4>
            <br>
            <h3>Walikukun Sport Center</h3>
        </center>

        <div class="page_break">
            <center>
                <h4>
                    Laporan Transaksi
                </h4>
                <br>
                <br>
                <table style="border-style:solid">
                    <thead>
                        <tr>
                            <th>
                                No.
                            </th>
                            <th>
                                Nama Customer
                            </th>
                            <th>
                                Jenis Paket
                            </th>
                            <th>
                                Jenis Customer
                            </th>
                            <th>
                                Discount
                            </th>
                            <th>
                                Waktu Checkin
                            </th>
                            <th>
                                Waktu Checkout
                            </th>
                            <th>
                                Durasi
                            </th>
                            <th>
                                Nominal Transaksi
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $no = 0;
                        @endphp
                        @foreach($data as $item)
                        @php
                            $no++;
                        @endphp
                        <tr>
                            <td>
                                {{$no}}
                            </td>
                            <td>
                                {{$item->customer->nama_customer}}
                            </td>
                            <td>
                                {{$item->paket->jenis_paket}}
                            </td>
                            <td>
                                {{$item->customer->flag_customer}}
                            </td>
                            <td>
                                {{$item->discount->jenis_discount ?? '-'}}
                            </td>
                            <td>
                                {{$item->waktu_chekin}}
                            </td>
                            <td>
                                {{$item->waktu_chekout}}
                            </td>
                            <td>
                                {{$item->durasi}}
                            </td>
                            <td>
                                Rp.{{number_format($item->nominal_transaksi)}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </center>
        </div>
        <div class="page_break">
            <table>
                <thead>
                    <tr>
                        <th>
                            No.
                        </th>
                        <th>
                            Nama Customer
                        </th>
                        <th>
                            Paket Pilihan
                        </th>
                        <th>
                            Jenis Customer
                        </th>
                        <th>
                            Nomor Member
                        </th>
                        <th>
                            Whatsapp
                        </th>
                        <th>
                            Email
                        </th>
                        <th>
                            Member Sejak
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $no = 0;
                    @endphp
                    @foreach($data as $item)
                    @php
                    $no++;
                    @endphp
                    <tr>
                        <td>{{$no}}</td>
                        <td>{{$item->customer->nama_customer}}</td>
                        <td>{{$item->paket->jenis_paket}}</td>
                        <td>{{$item->customer->flag_customer}}</td>
                        <td>{{$item->customer->no_member ?? '-'}}</td>
                        <td>{{$item->customer->no_wa_customer}}</td>
                        <td>{{$item->customer->email_customer}}</td>
                        <td>{{$item->customer->member_since ?? '-'}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</body>

</html>
