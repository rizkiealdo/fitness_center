@extends('layouts.app')
@section('title', 'Profil')
@section('content')
<div class="main_content_iner ">
    <div class="container-fluid p-0 sm_padding_15px">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="white_box mb_30">
                    <div class="box_header ">
                        <div class="main-title">
                            <h3 class="mb-0">profile Box 4</h3>
                        </div>
                    </div>
                    <div class="profile_card_5">
                        <div class="cover-photo">
                            <img src="{{ (Auth::user()->foto_profil) ? asset('storage/' . Auth::user()->foto_profil) : 'https://ui-avatars.com/api/?background=a0a0a0&name='.Auth::user()->name }}" class="profile">
                        </div>

                        <p class="about"></p>
                        <div class="h3">{{ Auth::user()->name }}</div>
                        <button class="btn btn-primary btn-password" data-bs-toggle="modal" data-bs-target="#modal-password">Ubah Password</button>
                        <button class="btn btn-primary btn-nama" data-bs-toggle="modal" data-bs-target="#modal-nama">Ubah Nama</button>
                        <button class="btn btn-primary btn-profil" data-bs-toggle="modal" data-bs-target="#exampleModal">Ubah Foto Profil</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Ubah Foto Profil</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="form-foto-profil" method="POST" action="{{url('/update-foto-profil')}}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="foto_profil" class="col-form-label">Foto Profil:</label>
                        <input type="hidden" name="id_user" value="{{ Auth::user()->id }}">
                        <input type="file" class="form-control" name="foto_profil" id="foto_profil">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-password" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Ubah Password</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <form id="form-password" method="POST" action="{{url('/update-password')}}">
                    @csrf
                    <label for="">Password Baru</label>
                    <input type="password" name="password_baru" id="password_baru" class="form-control mb-3" placeholder="********">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-nama" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Ubah Nama</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <form id="form-nama" method="POST" action="{{url('/update-nama')}}">
                    @csrf
                    <label for="">Nama</label>
                    <input type="text" name="nama_baru" id="nama_baru" class="form-control mb-3" placeholder="Nama Anda">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@push('script')
<script src="{{asset('js/page/profil.js')}}"></script>
@endpush
