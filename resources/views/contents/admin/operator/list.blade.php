@extends('layouts.app')
@section('title', 'Dashboard | Admin')
@section('content')
<div class="main_content_iner ">
    <div class="container-fluid p-0 sm_padding_15px">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="dashboard_header mb_50">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="dashboard_header_title">
                                <h3> Operator</h3>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="dashboard_breadcam text-end">
                                <button type="button" class="btn btn-primary" id="btn-tambah">Tambah Data</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <div class="transaksi-space">
                                    <div class="d-flex justify-content-center">
                                        <div class="spinner-border" role="status">
                                            <span class="visually-hidden">Loading...</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

{{-- Modal Check In Here --}}
<div class="modal fade" id="modal-checkin" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Pilih Jenis Paket</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    @forelse ($paket as $item)
                    <div class="col-sm-4">
                        <div class="card bg-dark text-white">
                            @if ($item->jenis_paket == 'Harian')
                            <img class="card-img" src="{{asset('img/face-scan.gif')}}" alt="Card image">
                            @elseif ($item->jenis_paket == 'Bulanan')
                            <img class="card-img" src="{{asset('img/user.gif')}}" alt="Card image">
                            @elseif ($item->jenis_paket == 'Bulanan + Trainer')
                            <img class="card-img" src="{{asset('img/seminar.gif')}}" alt="Card image">
                            @endif
                            <div class="card-img-overlay" style="background-color: #120133d5;">
                                <h5 class="card-title text-white">{{$item->jenis_paket}}</h5>
                                <p class="text-white">{{$item->keterangan_paket}}.</p>
                                <p class="text-white">Rp.{{ number_format($item->harga_paket, 0, ',', '.') }}</p>
                                <button class="btn btn-light btn-pilih-paket" data-jenis="{{$item->id_paket}}">Pilih Paket</button>
                            </div>
                        </div>
                    </div>
                    @empty

                    @endforelse
                </div>
            </div>

        </div>
    </div>
</div>
{{-- Modal Check In End --}}

{{-- Modal Check In Here --}}
<div class="modal fade" id="modal-pilih-form" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Checkin Customer</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div id="loading-form">
                    <div class="d-flex justify-content-center">
                        <div class="spinner-border text-primary" role="status">
                            <span class="visually-hidden">Loading...</span>
                        </div>
                    </div>
                </div>
                <div id="content-form">
                </div>
            </div>
        </div>
    </div>
</div>
{{-- Modal Check In End --}}
@endsection

@push('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/countup.js/2.0.0/countUp.min.js"></script>
<script src="{{asset('js/page/admin/operator/list.js')}}"></script>
@endpush
