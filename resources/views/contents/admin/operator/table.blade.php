<table class="table table-transaksi text-center" id="table-transaksi">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Customer</th>
            <th>Jenis Paket</th>
            <th>Discount</th>
            <th>Waktu Chekin</th>
            <th>Durasi</th>
            <th>Waktu Checkout</th>
            <th>Total Pembayaran</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        @php
        $no = 0;
        @endphp
        @forelse ($data as $item)
        @php
        $no++;
        @endphp
        <tr>
            <td>{{$no}}</td>
            <td>{{$item->nama_customer}}</td>
            <td>{{$item->jenis_paket}}</td>
            <td>{{$item->jenis_discount ?? '-'}}</td>
            <td class="starttime">{{$item->waktu_chekin}}</td>
            
            <td>
                <input type="text" name="durasi" class="durasi-input" id="durasi_{{$item->id_transaksi}}" @if($item->durasi == null) style="display: block" @elseif ($item->durasi != null) style="display: none" @endif readonly>
                <style>
                    .durasi-input {
                        border: none;
                        background-color: transparent;
                        padding: 0;
                        margin: 0;
                        font-family: inherit;
                        font-size: inherit;
                        outline: none;
                        cursor: default;
                    }
                </style>
                @if($item->durasi != null) {{$item->durasi}} @endif
                
            </td>
            <td class="endtime">
                {{$item->waktu_chekout}}
            </td>
            <td>Rp.{{number_format($item->nominal_transaksi) ?? '0'}}
                @if ($item->denda != null)
                    <br>
                    <span class="badge rounded-pill text-danger bg-soft-danger mb-2 py-2 px-4 fw-semibold">
                        Denda : Rp. {{number_format($item->denda)}}
                    </span>
                @endif
            </td>
            <td>
                <div class="btn-group" role="group" aria-label="Basic outlined example">
                    <button type="button" class="btn btn-outline-primary btn-checkout" data-id="{{$item->id_transaksi}}">
                        <i class='bx bxs-door-open'></i>
                    </button>
                    <a target="_blank" href="{{url('/admin/nota/?id_transaksi='.$item->id_transaksi)}}" class="btn btn-outline-success btn-cetak" data-id="{{$item->id_transaksi}}">
                        <i class='bx bxs-printer'></i>
                    </a>
                </div>
            </td>
        </tr>
        @empty
        @endforelse
    </tbody>
</table>
