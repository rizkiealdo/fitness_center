<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nota Transaksi</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Source+Code+Pro&display=swap" rel="stylesheet">

    <style>
        /* @import url('https://fonts.googleapis.com/css2?family=Source+Sans+Pro&display=swap'); */

        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            font-family: 'Source Code Pro', monospace;
        }

        .container {
            display: block;
            width: 100%;
            background: #fff;
            max-width: 350px;
            padding: 25px;
            margin: 25px auto 0;
            box-shadow: 0 3px 10px rgb(0 0 0 / 0.2);
        }

        .receipt_header {
            padding-bottom: 40px;
            border-bottom: 1px dashed #000;
            text-align: center;
        }

        .receipt_header h1 {
            font-size: 20px;
            margin-bottom: 5px;
            text-transform: uppercase;
        }

        .receipt_header h1 span {
            display: block;
            font-size: 25px;
        }

        .receipt_header h2 {
            font-size: 14px;
            color: #727070;
            font-weight: 300;
        }

        .receipt_header h2 span {
            display: block;
        }

        .receipt_body {
            margin-top: 25px;
        }

        table {
            width: 100%;
        }

        thead,
        tfoot {
            position: relative;
        }

        thead th:not(:last-child) {
            text-align: left;
        }

        thead th:last-child {
            text-align: right;
        }

        thead::after {
            content: '';
            width: 100%;
            border-bottom: 1px dashed #000;
            display: block;
            position: absolute;
        }

        tbody td:not(:last-child),
        tfoot td:not(:last-child) {
            text-align: left;
        }

        tbody td:last-child,
        tfoot td:last-child {
            text-align: right;
        }

        tbody tr:first-child td {
            padding-top: 15px;
        }

        tbody tr:last-child td {
            padding-bottom: 15px;
        }

        tfoot tr:first-child td {
            padding-top: 15px;
        }

        tfoot::before {
            content: '';
            width: 100%;
            border-top: 1px dashed #000;
            display: block;
            position: absolute;
        }

        tfoot tr:first-child td:first-child,
        tfoot tr:first-child td:last-child {
            font-weight: bold;
            font-size: 20px;
        }

        .date_time_con {
            display: flex;
            justify-content: center;
            column-gap: 25px;
        }

        .items {
            margin-top: 25px;
        }

        h3 {
            border-top: 1px dashed #000;
            padding-top: 10px;
            margin-top: 25px;
            text-align: center;
            text-transform: uppercase;
        }
    </style>
</head>

<body>

    <div class="container">

        <div class="receipt_header">
            <h3>Nota Transaksi 
                <br>
                <span>Walikukun Sport Center</span>
            </h3>
            <h4> Walikukun, Kec. Widodaren, Kab. Ngawi, Prov. Jawa Timur </h4>
        </div>

        <div class="receipt_body">

            <div class="date_time_con">
                <div class="date">{{Carbon\Carbon::now();}}</div>
            </div>

            <div class="items">
                <table>

                    <thead>
                        <th>NO</th>
                        <th>Transaksi</th>
                        <th>Detail</th>
                    </thead>

                    <tbody>
                        <tr>
                            <td>
                                1.
                            </td>
                            <td>
                                <strong>Jenis Paket</strong>
                            </td>
                            <td>
                                {{$data->paket->jenis_paket}}
                            </td>
                        </tr>

                        <tr>
                            <td>
                                2.
                            </td>
                            <td>
                                <strong>Discount</strong>
                            </td>
                            <td>
                                {{$data->discount ? $data->discount->jenis_discount . ' ('.$data->discount->discount.'%)' : '-'}}
                            </td>
                        </tr>

                        <tr>
                            <td>
                                3.
                            </td>
                            <td>
                                <strong>Waktu Checkin</strong>
                            </td>
                            <td>
                                {{$data->waktu_chekin}}
                            </td>
                        </tr>

                        <tr>
                            <td>
                                4.
                            </td>
                            <td>
                                <strong>Waktu Checkout</strong>
                            </td>
                            <td>
                                {{$data->waktu_chekout}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                5.
                            </td>
                            <td>
                                <strong>Durasi</strong>
                            </td>
                            <td>
                                {{$data->durasi}} Menit
                            </td>
                        </tr>
                        @if ($data->denda != null)
                            <tr>
                                <td>
                                    6.
                                </td>
                                <td>
                                    <strong>Denda</strong>
                                </td>
                                <td>
                                    Rp.{{number_format($data->denda)}}
                                </td>
                            </tr>
                        @endif
                    </tbody>

                    <tfoot>
                        <tr>
                            <td>
                                
                            </td>
                            <td>
                                <strong>Total</strong>
                            </td>
                            <td>
                                Rp. {{number_format($data->nominal_transaksi)}}
                            </td>
                        </tr>
                        @if($data->paket->id_paket == '2' || $data->paket->id_paket == 3)
                        <tr>
                            <td>
                                7.
                            </td>
                            <td>
                                <strong>Sisa Masa Langganan</strong>
                            </td>
                            <td>
                                {{$data->customer->sisa_langganan}}
                            </td>
                        </tr>
                        @endif
                    </tfoot>

                </table>
            </div>

        </div>


        <h3>Thank You!</h3>

    </div>

</body>

</html>
