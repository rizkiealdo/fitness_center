<form id="form-checkin" method="POST" action="{{ route('admin.store-transaksi') }}">
    @csrf
    <input type="hidden" name="id_paket" id="id_paket" value="3">
    <div class="row mb-3">
        <label for="no_member">Nomor Member</label>
        <input type="text" name="no_member" id="no_member" class="form-control" placeholder="Nomor Member">
    </div>
    <div class="row mb-3">
        <label for="id_customer">Nama Customer</label>
        <select name="id_customer" id="id_customer" class="form-select id_customer js-example-responsive">
            <option value="" selected disabled>-- Pilih Customer --</option>
            @forelse($customer as $item)
            <option value="{{ $item->id_customer }}">{{ $item->nama_customer }}</option>
            @empty
            @endforelse
        </select>
    </div>
    <div class="row mb-3">
        <div class="alert alert-info text-center" role="alert">
            With Trainer
        </div>
    </div>
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-primary">Simpan</button>
</form>
