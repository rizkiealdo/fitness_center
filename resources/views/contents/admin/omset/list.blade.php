@extends('layouts.app')
@section('title', 'Data Omset | Admin')
@section('content')
<div class="main_content_iner ">
    <div class="container-fluid p-0 sm_padding_15px">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="dashboard_header mb_50">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="dashboard_header_title">
                                <h3>Rekap Omset</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="card" style="border: none;">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-omset" id="table-omset">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Bulan</th>
                                            <th>Omset Bulanan</th>
                                            <th>Rata Rata Omset Harian</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                        $no = 0;
                                        @endphp
                                        @forelse ($data as $item)
                                        @php
                                            $no++;
                                        @endphp
                                        <tr>
                                            <td>{{$no}}</td>
                                            <td>{{$item->month_year}}</td>
                                            <td>Rp.{{number_format($item->omset)}}</td>
                                            <td>Rp.{{number_format($item->rata_rata)}}</td>
                                        </tr>
                                        @empty

                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@push('script')
<script src="{{asset('js/page/admin/omset/list.js')}}"></script>
@endpush
