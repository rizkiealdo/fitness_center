@extends('layouts.app')
@section('title', 'Data Alat | Admin')
@section('content')
<div class="main_content_iner ">
    <div class="container-fluid p-0 sm_padding_15px">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="dashboard_header mb_50">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="dashboard_header_title">
                                <h3>Data ALat</h3>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="dashboard_breadcam text-end">
                                <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                    data-bs-target="#modal-alat">
                                    Tambah Data
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-stripped" id="table-alat" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nama Alat</th>
                                            <th>Nomor Seri</th>
                                            <th>Teknisi</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-alat" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Tambah Data Alat</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="form-alat" action="{{route('admin.store-alat')}}" method="post">
                    @csrf
                    @method('post')
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" name="id_alat" id="id_alat">
                            <label for="nama_alat">Nama Alat</label>
                            <input type="text" name="nama_alat" id="nama_alat" class="form-control"
                                placeholder="Nama Alat">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="nomor_seri_alat">Nama Alat</label>
                            <input type="text" name="nomor_seri_alat" id="nomor_seri_alat" class="form-control"
                                placeholder="Nomor Seri">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="id_teknisi">Nama Teknisi</label>
                            <select name="id_teknisi" id="id_teknisi" class="form-select">
                                <option value="" selected disabled>--Pilih Teknisi --</option>
                                @forelse ($teknisi as $item)
                                <option value="{{$item->id_teknisi}}">{{$item->nama_teknisi}}</option>
                                @empty

                                @endforelse

                            </select>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

@endsection

@push('script')
<script src="{{asset('js/page/admin/alat/list.js')}}"></script>
@endpush
