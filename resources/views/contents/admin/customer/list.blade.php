@extends('layouts.app')
@section('title', 'Data Customer | Admin')
@section('content')
<div class="main_content_iner ">
    <div class="container-fluid p-0 sm_padding_15px">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="dashboard_header mb_50">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="dashboard_header_title">
                                <h3>Data Customer</h3>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="dashboard_breadcam text-end">
                                <button type="button" class="btn btn-primary" id="btn-tambah">Tambah Data</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="card" style="border: none;">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-member" id="table-member">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nama Customer</th>
                                            <th>Kategori</th>
                                            <th>Member Sejak</th>
                                            <th>Whatsapp</th>
                                            <th>Email</th>
                                            <th>Lihat Kartu Member</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

{{-- Modal Check In Here --}}
<div class="modal fade" id="modal-customer" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Data Customer</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="form-customer" method="POST" action="{{route('admin.simpan-customer')}}">
                    @csrf
                    <input type="hidden" name="id_customer" id="id_customer" class="id_customer">
                    <div class="mb-3">
                        <label for="nama_customer">Nama Customer</label>
                        <input type="text" name="nama_customer" id="nama_customer" class="form-control" placeholder="Nama Customer" required>
                    </div>
                    <div class="mb-3">
                        <label for="flag_customer">Tambahkan Sebagai Member / Non-Member</label>
                        <select name="flag_customer" id="flag_customer" class="form-control" required>
                            <option value="" selected disabled>-- Pilih --</option>
                            <option value="member">Member</option>
                            <option value="non-member">Non-member</option>
                        </select>
                    </div>
                    <div hidden class="mb-3 member">
                        <label for="id_paket">Jenis Paket Pilihan</label>
                        <select name="id_paket" id="id_paket" class="form-select">
                            <option value="" selected disabled>-- Pilih Jenis Paket --</option>
                            @forelse ($paket as $item)
                            <option value="{{$item->id_paket}}">{{$item->jenis_paket}}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>
                    <div hidden class="mb-3 member">
                        <label for="no_wa_customer">No. Whatsapp Customer</label>
                        <input type="number" name="no_wa_customer" id="no_wa_customer" class="form-control">
                    </div>
                    <div hidden class="mb-3 member">
                        <label for="email_customer">Email Customer</label>
                        <input type="text" name="email_customer" id="email_customer" class="form-control">
                    </div>
                    <div hidden class="mb-3 member">
                        <label for="member_since">Member Sejak</label>
                        <input type="text" name="member_since" id="member_since" class="form-control">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
{{-- Modal Check In End --}}

{{-- Modal Kartu Member --}}
<div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="imageModalLabel">Gambar Member</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="img-responsive">

                    <img id="imagePreview" class="img-fluid" src="" alt="Gambar Member">
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('script')
<script src="{{asset('js/page/admin/customer/list.js')}}"></script>
@endpush
