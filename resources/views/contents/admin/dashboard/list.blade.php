@extends('layouts.app')
@section('title', 'Dashboard | Admin')
@push('style')
<style>
    #chartdiv {
        width: 100%;
        height: 500px;
    }
</style>

<!-- Resources -->
<script src="https://cdn.amcharts.com/lib/5/index.js"></script>
<script src="https://cdn.amcharts.com/lib/5/xy.js"></script>
<script src="https://cdn.amcharts.com/lib/5/themes/Animated.js"></script>
@endpush
@section('content')
<div class="main_content_iner ">
    <div class="container-fluid p-0 sm_padding_15px">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="dashboard_header mb_50">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="dashboard_header_title">
                                <h3> Dashboard {{ Auth::user()->role->name }}</h3>
                            </div>
                        </div>
                        <div class="col-lg-6">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-xl-8">
                <div class="white_box mb_30">
                    <div class="box_header">
                        <div class="main-title">
                            <h3 class="mb_25">Rekapitulasi Pengunjung Fitness Setiap Bulan</h3>
                        </div>
                    </div>
                    <div id="chartdiv"></div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="list_counter_wrapper white_box mb_30 p-0 card_height_100">
                    <div class="single_list_counter">
                        <h3 class="deep_blue_2">
                            <span class="counter deep_blue_2 ">{{$total}}</span> +
                        </h3>
                        <p>Total Customer</p>
                    </div>
                    <div class="single_list_counter">
                        <h3 class="deep_blue_2"><span class="counter deep_blue_2">{{$member}}</span> + </h3>
                        <p>Total Member</p>
                    </div>
                    <div class="single_list_counter">
                        <h3 class="deep_blue_2"><span class="counter deep_blue_2">{{$non_member}}</span> + </h3>
                        <p>Total Non-Member</p>
                    </div>
                    <div class="single_list_counter">
                        <h3 class="deep_blue_2"><span class="counter deep_blue_2">{{$harian}}</span> + </h3>
                        <p>Rata Rata Pengunjung Per Hari</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script src="{{asset('js/page/admin/dashboard/list.js')}}"></script>
@endpush
