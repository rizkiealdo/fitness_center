@extends('layouts.app')
@section('title', 'History Aktivitas | Admin')
@section('content')
<div class="main_content_iner ">
    <div class="container-fluid p-0 sm_padding_15px">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="dashboard_header mb_50">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="dashboard_header_title">
                                <h3>History Aktivitas</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="card" style="border: none;">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-history" id="table-history">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Customer</th>
                                            <th>Jenis Paket</th>
                                            <th>Discount</th>
                                            <th>Waktu Chekin</th>
                                            <th>Durasi</th>
                                            <th>Waktu Checkout</th>
                                            <th>Total Pembayaran</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@push('script')
<script src="{{asset('js/page/admin/history/list.js')}}"></script>
@endpush
