@extends('layouts.app')
@section('title', 'Data Tekhnisi | Admin')
@section('content')
<div class="main_content_iner ">
    <div class="container-fluid p-0 sm_padding_15px">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="dashboard_header mb_50">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="dashboard_header_title">
                                <h3>Data Teknisi</h3>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="dashboard_breadcam text-end">
                                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal-teknisi">
                                    Tambah Data
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-stripped" id="table-teknisi" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nama Teknisi</th>
                                            <th>Nomor Whatsapp Teknisi</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-teknisi" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Tambah Data Teknisi</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="form-teknisi" action="{{route('admin.store-teknisi')}}" method="post">
                    @csrf
                    @method('post')
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" name="id_teknisi" id="id_teknisi">
                            <label for="nama_teknisi">Nama Teknisi</label>
                            <input type="text" name="nama_teknisi" id="nama_teknisi" class="form-control" placeholder="Nama Teknisi">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="nomor_wa_teknisi">Nomor Whatsapp Teknisi</label>
                            <input type="number" name="nomor_wa_teknisi" id="nomor_wa_teknisi" class="form-control" placeholder="Nomor Whatsapp">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Whatsapp -->
<div class="modal fade" id="modal-wa" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Kirim Pesan Ke Teknisi</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row mb-3">
                    <div class="col-md-12">
                        <label for="nomor_wa">Nomor Whatsapp</label>
                        <input type="text" name="no_pesan" id="no_pesan" class="form-control" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="pesan">Pesan</label>
                        <textarea name="pesan_teknisi" id="pesan_teknisi" cols="30" rows="10" class="form-control"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success btn-kirim">Kirim</button>
            </div>
        </div>
    </div>
</div>

@endsection

@push('script')
<script src="{{asset('js/page/admin/teknisi/list.js')}}"></script>
@endpush
