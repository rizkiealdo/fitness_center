<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard ~ Kementerian Pariwisata dan Ekonomi Kreatif - Deputi Bidang Industri dan Investasi</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="author" content="Kementerian Pariwisata dan Ekonomi Kreatif - Deputi Bidang Industri dan Investasi">
    <meta property="og:site_name"
        content="Kementerian Pariwisata dan Ekonomi Kreatif - Deputi Bidang Industri dan Investasi">
    <meta property="og:title"
        content="Dashboard ~ Kementerian Pariwisata dan Ekonomi Kreatif - Deputi Bidang Industri dan Investasi">
    <meta property="og:image" content="{{asset('themes/sisupar/assets/landing/img/logos/logo_kemenparekraf.png')}}">
    <meta property="og:url" content="https://sisupar.kemenparekraf.go.id/dashboard">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:image:alt"
        content="Kementerian Pariwisata dan Ekonomi Kreatif - Deputi Bidang Industri dan Investasi">
    <link rel="shortcut icon" type="image/x-icon"
        href="{{asset('themes/sisupar/assets/landing/img/others/favicon.ico')}}">
    <!-- Font -->
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;600&display=swap" rel="stylesheet">
    <!-- Google Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com/">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css"
        integrity="sha512-q3eWabyZPc1XTCmF+8/LuE1ozpg5xxn7iO89yfSOd5/oKvyqLngoNGsx8jq92Y8eXJ/IRxQbEC+FGSYxtk2oiw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link
        href="https://fonts.googleapis.com/css2?family=Exo:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap"
        rel="stylesheet">


    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet"
        href="https://sisupar.kemenparekraf.go.id/combine/2a40239b5ce36eac4853646b62189c9d-1667137139.css">

    <link href="https://sisupar.kemenparekraf.go.id/themes/sisupar/assets/dashboard/css/theme.min.css"
        data-hs-appearance="default" as="style">
    <link href="https://sisupar.kemenparekraf.go.id/themes/sisupar/assets/dashboard/css/theme-dark.min.css"
        data-hs-appearance="dark" as="style">

    <!-- CSS Front Template -->
    <link rel="stylesheet" href="https://sisupar.kemenparekraf.go.id/themes/sisupar/assets/dashboard/css/custom.css">
    <link href="https://sisupar.kemenparekraf.go.id/themes/sisupar/assets/js/sweetalert/sweetalert2.min.css"
        rel="stylesheet" type="text/css">

    <script src="https://sisupar.kemenparekraf.go.id/themes/sisupar/assets/dashboard/vendor/jquery/dist/jquery.min.js">
    </script>
    <style data-hs-appearance-onload-styles>
        * {
            transition: unset !important;
        }

        body {
            opacity: 0;
        }
    </style>

    <script>
        window.hs_config = {
            "autopath": "@@autopath",
            "deleteLine": "hs-builder:delete",
            "deleteLine:build": "hs-builder:build-delete",
            "deleteLine:dist": "hs-builder:dist-delete",
            "previewMode": false,
            "startPath": "/index.html",
            "vars": {
                "themeFont": "https://fonts.googleapis.com/css2?family=Inter:wght@400;600&display=swap",
                "version": "?v=1.0"
            },
            "layoutBuilder": {
                "extend": {
                    "switcherSupport": true
                },
                "header": {
                    "layoutMode": "default",
                    "containerMode": "container-fluid"
                },
                "sidebarLayout": "default"
            },
            "themeAppearance": {
                "layoutSkin": "default",
                "sidebarSkin": "default",
                "styles": {
                    "colors": {
                        "primary": "#377dff",
                        "transparent": "transparent",
                        "white": "#fff",
                        "dark": "132144",
                        "gray": {
                            "100": "#f9fafc",
                            "900": "#1e2022"
                        }
                    },
                    "font": "Inter"
                }
            },
            "languageDirection": {
                "lang": "en"
            },
            "skipFilesFromBundle": {
                "dist": ["assets/js/hs.theme-appearance.js", "assets/js/hs.theme-appearance-charts.js", "assets/js/demo.js"],
                "build": ["assets/css/theme.css", "assets/vendor/hs-navbar-vertical-aside/dist/hs-navbar-vertical-aside-mini-cache.js", "assets/js/demo.js", "assets/css/theme-dark.css", "assets/css/docs.css", "assets/vendor/icon-set/style.css", "assets/js/hs.theme-appearance.js", "assets/js/hs.theme-appearance-charts.js", "node_modules/chartjs-plugin-datalabels/dist/chartjs-plugin-datalabels.min.js", "assets/js/demo.js"]
            },
            "minifyCSSFiles": ["assets/css/theme.css", "assets/css/theme-dark.css"],
            "copyDependencies": {
                "dist": {
                    "*assets/js/theme-custom.js": ""
                },
                "build": {
                    "*assets/js/theme-custom.js": "",
                    "node_modules/bootstrap-icons/font/*fonts/**": "assets/css"
                }
            },
            "buildFolder": "",
            "replacePathsToCDN": {},
            "directoryNames": {
                "src": "./src",
                "dist": "./dist",
                "build": "./build"
            },
            "fileNames": {
                "dist": {
                    "js": "theme.min.js",
                    "css": "theme.min.css"
                },
                "build": {
                    "css": "theme.min.css",
                    "js": "theme.min.js",
                    "vendorCSS": "vendor.min.css",
                    "vendorJS": "vendor.min.js"
                }
            },
            "fileTypes": "jpg|png|svg|mp4|webm|ogv|json"
        }
        window.hs_config.gulpRGBA = (p1) => {
            const options = p1.split(',')
            const hex = options[0].toString()
            const transparent = options[1].toString()

            var c;
            if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
                c = hex.substring(1).split('');
                if (c.length == 3) {
                    c = [c[0], c[0], c[1], c[1], c[2], c[2]];
                }
                c = '0x' + c.join('');
                return 'rgba(' + [(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',') + ',' + transparent + ')';
            }
            throw new Error('Bad Hex');
        }
        window.hs_config.gulpDarken = (p1) => {
            const options = p1.split(',')

            let col = options[0].toString()
            let amt = -parseInt(options[1])
            var usePound = false

            if (col[0] == "#") {
                col = col.slice(1)
                usePound = true
            }
            var num = parseInt(col, 16)
            var r = (num >> 16) + amt
            if (r > 255) {
                r = 255
            } else if (r < 0) {
                r = 0
            }
            var b = ((num >> 8) & 0x00FF) + amt
            if (b > 255) {
                b = 255
            } else if (b < 0) {
                b = 0
            }
            var g = (num & 0x0000FF) + amt
            if (g > 255) {
                g = 255
            } else if (g < 0) {
                g = 0
            }
            return (usePound ? "#" : "") + (g | (b << 8) | (r << 16)).toString(16)
        }
        window.hs_config.gulpLighten = (p1) => {
            const options = p1.split(',')

            let col = options[0].toString()
            let amt = parseInt(options[1])
            var usePound = false

            if (col[0] == "#") {
                col = col.slice(1)
                usePound = true
            }
            var num = parseInt(col, 16)
            var r = (num >> 16) + amt
            if (r > 255) {
                r = 255
            } else if (r < 0) {
                r = 0
            }
            var b = ((num >> 8) & 0x00FF) + amt
            if (b > 255) {
                b = 255
            } else if (b < 0) {
                b = 0
            }
            var g = (num & 0x0000FF) + amt
            if (g > 255) {
                g = 255
            } else if (g < 0) {
                g = 0
            }
            return (usePound ? "#" : "") + (g | (b << 8) | (r << 16)).toString(16)
        }
    </script>
    <style>
        td {
            padding: .15rem .15rem !important;
        }
    </style>
</head>

<body>
    <script src="https://sisupar.kemenparekraf.go.id/themes/sisupar/assets/dashboard/js/hs.theme-appearance.js">
    </script>
    @if ( Auth::user()->role_id == 1 )
        @include('layouts.component.dashboard.header.list')
    @elseif( Auth::user()->role_id == 2 )
        @include('layouts.component.dashboard.header-lsup.list')
    @endif
    
    <main id="content" role="main" class="main">
        @yield('content')
    </main>


    <!-- JS Global Compulsory  -->
    <script src="https://sisupar.kemenparekraf.go.id/themes/sisupar/assets/dashboard/vendor/jquery/dist/jquery.min.js">
    </script>
    <script
        src="https://sisupar.kemenparekraf.go.id/themes/sisupar/assets/dashboard/vendor/jquery-migrate/dist/jquery-migrate.min.js">
    </script>
    <script
        src="https://sisupar.kemenparekraf.go.id/themes/sisupar/assets/dashboard/vendor/bootstrap/dist/js/bootstrap.bundle.min.js">
    </script>

    <!-- JS Implementing Plugins -->
    <script src="https://sisupar.kemenparekraf.go.id/combine/2e46e9b9b740c684e0520f0391db26d2-1667137139.js"></script>

    <!-- JS Front -->
    <script
        src="https://sisupar.kemenparekraf.go.id/themes/sisupar/assets/dashboard/vendor/tom-select/dist/js/tom-select.complete.min.js">
    </script>
    <script src="https://sisupar.kemenparekraf.go.id/themes/sisupar/assets/dashboard/js/theme.min.js"></script>
    <script src="https://sisupar.kemenparekraf.go.id/themes/sisupar/assets/js/sweetalert/sweetalert2.min.js"></script>

    <!-- JS Plugins Init. -->
    <script>
        (function() {
            // INITIALIZATION OF BOOTSTRAP DROPDOWN
            // =======================================================
            HSBsDropdown.init()


            // INITIALIZATION OF MEGA MENU
            // =======================================================
            new HSMegaMenu('.js-mega-menu', {
                desktop: {
                    position: 'left'
                }
            })


            // INITIALIZATION OF FORM SEARCH
            // =======================================================
            new HSFormSearch('.js-form-search')

            // INITIALIZATION OF SELECT
            // =======================================================
            HSCore.components.HSTomSelect.init('.js-select')
        })()
    </script>

    <!-- Style Switcher JS -->
    <script>
        (function() {
            // STYLE SWITCHER
            // =======================================================
            const $dropdownBtn = document.getElementById('selectThemeDropdown') // Dropdowon trigger
            const $variants = document.querySelectorAll(`[aria-labelledby="selectThemeDropdown"] [data-icon]`) // All items of the dropdown

            // Function to set active style in the dorpdown menu and set icon for dropdown trigger
            const setActiveStyle = function() {
                $variants.forEach($item => {
                    if ($item.getAttribute('data-value') === HSThemeAppearance.getOriginalAppearance()) {
                        $dropdownBtn.innerHTML = `<i class="${$item.getAttribute('data-icon')}" />`
                        return $item.classList.add('active')
                    }

                    $item.classList.remove('active')
                })
            }

            // Add a click event to all items of the dropdown to set the style
            $variants.forEach(function($item) {
                $item.addEventListener('click', function() {
                    HSThemeAppearance.setAppearance($item.getAttribute('data-value'))
                })
            })

            // Call the setActiveStyle on load page
            setActiveStyle()

            // Add event listener on change style to call the setActiveStyle function
            window.addEventListener('on-hs-appearance-change', function() {
                setActiveStyle()
            })
        })()
    </script>
    <!-- End Style Switcher JS -->

    <script src="https://sisupar.kemenparekraf.go.id/themes/sisupar/assets/dashboard/vendor/chart.js/dist/Chart.min.js">
    </script>
    <script type='text/javascript' src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        $(document).on('ready', function() {
            // INITIALIZATION OF DATERANGEPICKER
            // =======================================================
            $('.js-daterangepicker').daterangepicker();

            $('.js-daterangepicker-times').daterangepicker({
                timePicker: true,
                startDate: moment().startOf('hour'),
                endDate: moment().startOf('hour').add(32, 'hour'),
                locale: {
                    format: 'M/DD hh:mm A'
                }
            });

            var start = moment();
            var end = moment();

            function cb(start, end) {
                $('#js-daterangepicker-predefined .js-daterangepicker-predefined-preview').html(start.format('MMM D') + ' - ' + end.format('MMM D, YYYY'));
            }

            $('#js-daterangepicker-predefined').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end);
        });

        (function() {
            window.onload = function() {

                // INITIALIZATION OF BOOTSTRAP DROPDOWN
                // =======================================================
                HSBsDropdown.init()


                // INITIALIZATION OF CHARTJS
                // =======================================================
                HSCore.components.HSChartJS.init('.js-chart')
            }
        })()
    </script>
    <script type="text/javascript">
        google.charts.load('current', {
            'packages': ['geochart'],
            'mapsApiKey': ""
        });
        google.charts.setOnLoadCallback(drawRegionsMap);

        var provinceGeoChartData = "[[&quot;ID-AC&quot;,&quot;ACEH&quot;,1127,0],[&quot;ID-BA&quot;,&quot;BALI&quot;,5399,0],[&quot;ID-BT&quot;,&quot;BANTEN&quot;,8923,0],[&quot;ID-BE&quot;,&quot;BENGKULU&quot;,365,0],[&quot;ID-YO&quot;,&quot;DAERAH ISTIMEWA YOGYAKARTA&quot;,3773,0],[&quot;ID-JK&quot;,&quot;DKI JAKARTA&quot;,17731,0],[&quot;ID-GO&quot;,&quot;GORONTALO&quot;,264,0],[&quot;ID-JA&quot;,&quot;JAMBI&quot;,1179,0],[&quot;ID-JB&quot;,&quot;JAWA BARAT&quot;,23233,0],[&quot;ID-JT&quot;,&quot;JAWA TENGAH&quot;,12893,0],[&quot;ID-JI&quot;,&quot;JAWA TIMUR&quot;,17899,0],[&quot;ID-KB&quot;,&quot;KALIMANTAN BARAT&quot;,1596,0],[&quot;ID-KS&quot;,&quot;KALIMANTAN SELATAN&quot;,1306,0],[&quot;ID-KT&quot;,&quot;KALIMANTAN TENGAH&quot;,604,0],[&quot;ID-KI&quot;,&quot;KALIMANTAN TIMUR&quot;,2912,0],[&quot;ID-KU&quot;,&quot;KALIMANTAN UTARA&quot;,712,0],[&quot;ID-BB&quot;,&quot;KEPULAUAN BANGKA BELITUNG&quot;,1016,0],[&quot;ID-KR&quot;,&quot;KEPULAUAN RIAU&quot;,1721,0],[&quot;ID-LA&quot;,&quot;LAMPUNG&quot;,1517,0],[&quot;ID-MA&quot;,&quot;MALUKU&quot;,329,0],[&quot;ID-MU&quot;,&quot;MALUKU UTARA&quot;,521,0],[&quot;ID-NB&quot;,&quot;NUSA TENGGARA BARAT&quot;,1119,0],[&quot;ID-NT&quot;,&quot;NUSA TENGGARA TIMUR&quot;,1045,0],[&quot;ID-PA&quot;,&quot;PAPUA&quot;,812,0],[&quot;ID-PB&quot;,&quot;PAPUA BARAT&quot;,288,0],[&quot;ID-RI&quot;,&quot;RIAU&quot;,1952,0],[&quot;ID-SR&quot;,&quot;SULAWESI BARAT&quot;,289,0],[&quot;ID-SN&quot;,&quot;SULAWESI SELATAN&quot;,2749,0],[&quot;ID-ST&quot;,&quot;SULAWESI TENGAH&quot;,756,0],[&quot;ID-SG&quot;,&quot;SULAWESI TENGGARA&quot;,749,0],[&quot;ID-SA&quot;,&quot;SULAWESI UTARA&quot;,848,0],[&quot;ID-SB&quot;,&quot;SUMATERA BARAT&quot;,2485,0],[&quot;ID-SS&quot;,&quot;SUMATERA SELATAN&quot;,1289,0],[&quot;ID-SU&quot;,&quot;SUMATERA UTARA&quot;,4392,0]]";

        provinceGeoChartData = provinceGeoChartData.replace(/\\n/g, "\\n")
            .replace(/&quot;/g, '"')
            .replace(/\\'/g, "\\'")
            .replace(/\\"/g, '\\"')
            .replace(/\\&/g, "\\&")
            .replace(/\\r/g, "\\r")
            .replace(/\\t/g, "\\t")
            .replace(/\\b/g, "\\b")
            .replace(/\\f/g, "\\f")
            .replace(/[\u0000-\u0019]+/g, "");
        provinceGeoChartData = JSON.parse(provinceGeoChartData);

        var provinceGeoChartDataArray = [];

        for (var i = 0; i < provinceGeoChartData.length; ++i) {
            if (provinceGeoChartData[i]) {
                provinceGeoChartDataArray.push([{
                    v: provinceGeoChartData[i][0],
                    f: provinceGeoChartData[i][1]
                }, provinceGeoChartData[i][2], provinceGeoChartData[i][3]]);
            }
        }

        function drawRegionsMap() {
            var data = new google.visualization.arrayToDataTable([]);

            data.addColumn('string', 'Province');
            data.addColumn('number', "Jumlah Usaha BKPM");
            data.addColumn('number', "Jumlah Usaha Tersertifikasi");

            data.addRows(provinceGeoChartDataArray);

            var options = {
                region: 'ID',
                resolution: 'provinces',
                colorAxis: {
                    colors: ['#f5f5f5', '#15264c']
                },
                backgroundColor: '#fff',
                datalessRegionColor: '#f5f5f5',
                defaultColor: '#f5f5f5',
            };

            var chart = new google.visualization.GeoChart(document.getElementById('provinces-geo-chart'));

            chart.draw(data, options);
        }

        function numberWithDots(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        }
    </script>
    <script src="{{asset('themes/modules/assets/js/framework-extras.js')}}"></script>
    <link rel="stylesheet" property="stylesheet" href="{{asset('themes/modules/assets/css/framework-extras.css')}}">
    <script>
        $(window).on('ajaxErrorMessage', function(event, message) {
            $.oc.flashMsg({
                text: message,
                'class': 'error',
                'interval': 10
            });
            return false;
            event.preventDefault();
        });
    </script>

</body>

</html>