<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" />

    <title>@yield('title')</title>
    <style>
        .bg-image-vertical {
        position: relative;
        overflow: hidden;
        background-repeat: no-repeat;
        background-position: right center;
        background-size: auto 100%;
        }
        
        @media (min-width: 1025px) {
        .h-custom-2 {
        height: 100%;
        }
        }
    </style>
</head>
<body>
    @yield('content')

    <script src="{{asset('js/bootstrap1.min.js')}}"></script>
    <script src="{{asset('js/login.js')}}"></script>
</body>
</html>