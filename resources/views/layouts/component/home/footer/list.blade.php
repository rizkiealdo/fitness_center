<!-- Footer -->
<footer>
    <div class="container-fluid d-none d-sm-block">
        <!-- SVG Background -->
        <figure class="w-100">
            <img class="" style="width: 100% !important; height: auto;" src="{{asset('themes/sisupar/assets/landing/svg/components/irregular-shape-2-bottom.svg')}}">
        </figure>
        <!-- End SVG Background Section -->
    </div>

    <!-- ========== FOOTER ========== -->
    <footer class="bg-primary">
        <div class="container pb-1 pb-lg-5">
            <div class="row content-space-t-2">
                <div class="col-lg-6 mb-7 mb-lg-0">
                    <!-- Logo -->
                    <div class="mb-5">
                        <a class="navbar-brand" href="index.html" aria-label="Space">
                            <img class="logo-footer" src="{{asset('themes/sisupar/assets/images/logos/logo-group-white.png')}}" alt="Image Description">
                        </a>
                    </div>
                    <!-- End Logo -->

                    <!-- List -->
                    <ul class="list-unstyled list-py-1">
                        <li><a class="link-sm link-light" href="#"><i class="bi-geo-alt-fill me-1"></i> Kementerian BUMN Lt.15</a></li>
                        <li><a class="link-sm link-light" href="tel:1-062-109-9222"><i class="bi-envelope me-1"></i> usahapar@kemenparekraf.go.id</a></li>
                    </ul>
                    <!-- End List -->

                </div>
                <!-- End Col -->

                <div class="col-sm-3 mb-7 mb-lg-0">
                    <h5 class="text-white mb-3">Bantuan</h5>

                    <!-- List -->
                    <ul class="list-unstyled list-py-1 mb-0">
                        <li><a class="link-sm link-light" href="pertanyaan-umum.html">Pertanyaan Umum</a></li>
                        <li><a class="link-sm link-light" href="hubungi-kami.html">Hubungi Kami</a></li>
                    </ul>
                    <!-- End List -->
                </div>
                <!-- End Col -->

                <div class="col-sm-3 mb-7 mb-lg-0">
                    <h5 class="text-white mb-3">Tautan Terkait</h5>

                    <!-- List -->
                    <ul class="list-unstyled list-py-1 mb-0">
                        <li><a class="link-sm link-light" target="_blank" href="https://kemenparekraf.go.id/">Website Resmi</a></li>
                        <li><a class="link-sm link-light" target="_blank" href="https://www.indonesia.travel/gb/en/home">Wonderful Indonesia</a></li>
                    </ul>
                    <!-- End List -->
                </div>
                <!-- End Col -->
            </div>
            <!-- End Row -->

            <div class="border-top border-white-10 my-7"></div>

            <div class="row mb-7">
                <div class="col-sm mb-3 mb-sm-0">

                </div>

                <div class="col-sm-auto">
                    <!-- Socials -->
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item">
                            <a class="btn btn-soft-light btn-xs btn-icon" href="https://www.facebook.com/ParekrafRI/">
                                <i class="bi-facebook"></i>
                            </a>
                        </li>

                        <li class="list-inline-item">
                            <a class="btn btn-soft-light btn-xs btn-icon" href="https://www.instagram.com/kemenparekraf.ri/">
                                <i class="bi-instagram"></i>
                            </a>
                        </li>

                        <li class="list-inline-item">
                            <a class="btn btn-soft-light btn-xs btn-icon" href="https://twitter.com/Kemenparekraf">
                                <i class="bi-twitter"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- End Socials -->
                </div>
            </div>

            <!-- Copyright -->
            <div class="w-md-85 text-lg-center mx-lg-auto">
                <p class="text-white-50 small">&copy; 2022 Kementerian Pariwisata dan Ekonomi Kreatif / Badan Pariwisata dan Ekonomi Kreatif.</p>
            </div>
            <!-- End Copyright -->
        </div>
    </footer>
</footer>