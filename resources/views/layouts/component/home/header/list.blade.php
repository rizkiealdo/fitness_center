<header id="header" class="navbar navbar-expand-lg navbar-end navbar-absolute-top navbar-light header-scroll navbar-show-hide " data-hs-header-options='{
   "fixMoment": 200,
   "fixEffect": "slide"
   }'>
    <div class="container">
        <nav class="js-mega-menu navbar-nav-wrap">
            <!-- Default Logo -->
            <a class="navbar-brand d-flex flex-row gap-3" href="index.html" aria-label="Front">
                <img class="navbar-brand-logo" src="{{asset('themes/sisupar/assets/images/logos/logo-group-white.png')}}" alt="Logo KemenparEkraf">
                <span class="navbar-brand-text text-white d-none d-sm-block">KEMENTERIAN PARIWISATA DAN EKONOMI KREATIF/<br>BADAN PARIWISATA DAN EKONOMI KREATIF</span>
            </a>
            <!-- End Default Logo -->
            <!-- Toggler -->
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-default">
                    <i class="bi-list"></i>
                </span>
                <span class="navbar-toggler-toggled">
                    <i class="bi-x"></i>
                </span>
            </button>
            <!-- End Toggler -->
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <div class="navbar-absolute-top-scroller header-scroll header-scroll-bg ">
                    <ul class="navbar-nav">
                        <!-- Landings -->
                        <li class="hs-has-mega-menu nav-item">
                            <a id="directoryMegaMenu" class="hs-mega-menu-invoker nav-link dropdown-toggle text-navbar-custom active" aria-current="page" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Direktori</a>
                            <!-- Mega Menu -->
                            <div class="hs-mega-menu dropdown-menu w-100" aria-labelledby="directoryMegaMenu" style="min-width: 30rem;">
                                <div class="row">
                                    <div class="col-lg-4 d-none d-lg-block">
                                        <!-- Banner Image -->
                                        <div class="navbar-dropdown-menu-banner" style="background-image: url('themes/sisupar/assets/landing/img/others/bg-menu-direktori.jpg');">
                                            <div class="navbar-dropdown-menu-banner-content">
                                                <div class="mb-4">
                                                    <span class="h2 d-block text-white">Direktori Usaha</span>
                                                    <p class="text-accent">Daftar Jenis Usaha</p>
                                                </div>
                                                <a class="btn btn-primary btn-transition" href="usaha-tersertifikasi/direktori.html">Selengkapnya <i class="bi-chevron-right small"></i></a>
                                            </div>
                                        </div>
                                        <!-- End Banner Image -->
                                    </div>
                                    <!-- End Col -->
                                    <div class="col-lg-8">
                                        <div class="navbar-dropdown-menu-inner">
                                            <div class="row">
                                                <div class="col-md-3 mb-3 mb-sm-0">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/angkutan-jalan-rel-wisata.html" title="Angkutan Jalan Rel Wisata">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/635/3d6/808/6353d6808bed8546361423.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Angkutan Jalan Rel Wisata">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Angkutan Jalan Rel Wisata
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3 mb-3 mb-sm-0">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/angkutan-laut-dalam-negeri-untuk-wisata-tidak-berakomodasi.html" title="Angkutan Laut Dalam Negeri Untuk Wisata (Tidak Berakomodasi)">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/635/60c/868/63560c8685025029969296.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Angkutan Laut Dalam Negeri Untuk Wisata (Tidak Berakomodasi)">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Angkutan Laut Dalam Negeri Untuk Wisata (Tidak Berakomodasi)
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3 mb-3 mb-sm-0">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/angkutan-wisata-di-sungai-dan-danau-untuk-wisata-dan-yang-berhubungan-dengan-itu.html" title="Angkutan Wisata di Sungai Dan Danau Untuk Wisata Dan Yang Berhubungan Dengan Itu">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/610/76d/f2c/61076df2c6e92008414512.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Angkutan Wisata di Sungai Dan Danau Untuk Wisata Dan Yang Berhubungan Dengan Itu">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Angkutan Wisata di Sungai Dan Danau Untuk Wisata Dan Yang Berhubungan Dengan Itu
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3 mb-3 mb-sm-0">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/apartemen-hotel-berisiko-tinggi.html" title="Apartemen Hotel Berisiko Tinggi">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/635/3b2/d48/6353b2d480b71621722956.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Apartemen Hotel Berisiko Tinggi">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Apartemen Hotel Berisiko Tinggi
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3 mb-3 mb-sm-0">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/bar.html" title="Bar">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/610/76c/cb0/61076ccb0d763565290844.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Bar">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Bar
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3 mb-3 mb-sm-0">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/diskotek.html" title="Diskotek">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/610/76c/df1/61076cdf1ac60990838239.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Diskotek">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Diskotek
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3 mb-3 mb-sm-0">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/fasilitasi-gelanggangarena-biliar.html" title="Fasilitasi Gelanggang/Arena Biliar">
                                                        <div class="flex-0">
                                                            <img class="" src="{{('uploads/public/610/76b/e67/61076be67e22a875882354.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Fasilitasi Gelanggang/Arena Biliar">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Fasilitasi Gelanggang/Arena Biliar
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3 mb-3 mb-sm-0">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/fasilitasi-gelanggangarena-hoki-es.html" title="Fasilitasi Gelanggang/Arena Hoki Es">
                                                        <div class="flex-0">
                                                            <img class="" src="{{('uploads/public/635/602/297/6356022971658278576332.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Fasilitasi Gelanggang/Arena Hoki Es">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Fasilitasi Gelanggang/Arena Hoki Es
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3 mb-3 mb-sm-0">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/fasilitasi-gelanggangarena-renang.html" title="Fasilitasi Gelanggang/Arena Renang">
                                                        <div class="flex-0">
                                                            <img class="" src="{{('uploads/public/610/76c/037/61076c037ba93086764746.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Fasilitasi Gelanggang/Arena Renang">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Fasilitasi Gelanggang/Arena Renang
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3 mb-3 mb-sm-0">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/hotel-bintang-berisiko-menengah-tinggi.html" title="Hotel Bintang Berisiko Menengah Tinggi">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/636/0b2/1ed/6360b21ed6ac7151243453.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Hotel Bintang Berisiko Menengah Tinggi">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Hotel Bintang Berisiko Menengah Tinggi
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3 mb-3 mb-sm-0">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/hotel-melati-berisiko-menengah-tinggi.html" title="Hotel Melati Berisiko Menengah Tinggi">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/638/5d6/af0/6385d6af05088420969274.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Hotel Melati Berisiko Menengah Tinggi">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Hotel Melati Berisiko Menengah Tinggi
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3 mb-3 mb-sm-0">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/jasa-boga-untuk-suatu-event-tertentu-event-catering.html" title="Jasa Boga Untuk Suatu Event Tertentu (Event Catering)">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/635/3be/eb2/6353beeb21788139609563.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Jasa Boga Untuk Suatu Event Tertentu (Event Catering)">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Jasa Boga Untuk Suatu Event Tertentu (Event Catering)
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3 mb-3 mb-sm-0">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/kelab-malam-atau-diskotik-yang-utamanya-menyediakan-minuman.html" title="Kelab Malam Atau Diskotik Yang Utamanya Menyediakan Minuman">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/610/76d/146/61076d146b549403140019.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Kelab Malam Atau Diskotik Yang Utamanya Menyediakan Minuman">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Kelab Malam Atau Diskotik Yang Utamanya Menyediakan Minuman
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3 mb-3 mb-sm-0">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/lapangan-golf.html" title="Lapangan Golf">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/610/769/712/610769712fa68004180214.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Lapangan Golf">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Lapangan Golf
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3 mb-3 mb-sm-0">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/penyediaan-jasa-boga-periode-tertentu.html" title="Penyediaan Jasa Boga Periode Tertentu">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/635/3c0/0dd/6353c00dd96c3031851903.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Penyediaan Jasa Boga Periode Tertentu">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Penyediaan Jasa Boga Periode Tertentu
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3 mb-3 mb-sm-0">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/restoran-berisiko-tinggi.html" title="Restoran Berisiko Tinggi">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/610/76d/318/61076d318d491763681127.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Restoran Berisiko Tinggi">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Restoran Berisiko Tinggi
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3 mb-3 mb-sm-0">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/sante-par-aqua-spa-tirta-2.html" title="Sante Par Aqua (Spa) Tirta 2">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/635/39c/006/63539c006794e138897464.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Sante Par Aqua (Spa) Tirta 2">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Sante Par Aqua (Spa) Tirta 2
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3 mb-3 mb-sm-0">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/taman-rekreasi.html" title="Taman Rekreasi">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/610/769/8a2/6107698a26287734015483.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Taman Rekreasi">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Taman Rekreasi
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3 mb-3 mb-sm-0">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/vila-bintang-3-diamond.html" title="Vila Bintang 3 (Diamond)">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/610/76d/598/61076d598cc75482573559.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Vila Bintang 3 (Diamond)">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Vila Bintang 3 (Diamond)
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3 mb-3 mb-sm-0">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/wisata-memancing.html" title="Wisata Memancing">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/610/769/a23/610769a23a6f1664123341.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Wisata Memancing">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Wisata Memancing
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3 mb-3 mb-sm-0">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/wisata-selam.html" title="Wisata Selam">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/610/76d/733/61076d733202b445970224.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Wisata Selam">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Wisata Selam
                                                        </span>
                                                    </a>
                                                </div>

                                                <!-- End Col -->

                                                <div class="col-md-3">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/aktivitas-wisata-air.html" title="Aktivitas Wisata Air">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/637/c31/bb9/637c31bb9f0c1807406379.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Aktivitas Wisata Air">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Aktivitas Wisata Air
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/angkutan-laut-dalam-negeri-untuk-wisata-berakomodasi.html" title="Angkutan Laut Dalam Negeri Untuk Wisata (Berakomodasi)">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/610/76d/c5c/61076dc5c18bb147050462.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Angkutan Laut Dalam Negeri Untuk Wisata (Berakomodasi)">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Angkutan Laut Dalam Negeri Untuk Wisata (Berakomodasi)
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/angkutan-laut-luar-negeri-untuk-wisata.html" title="Angkutan Laut Luar Negeri Untuk Wisata">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/610/76d/d95/61076dd9545d7665494274.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Angkutan Laut Luar Negeri Untuk Wisata">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Angkutan Laut Luar Negeri Untuk Wisata
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/apartemen-hotel-berisiko-menengah-tinggi.html" title="Apartemen Hotel Berisiko Menengah Tinggi">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/635/3b2/61c/6353b261c3797169559791.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Apartemen Hotel Berisiko Menengah Tinggi">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Apartemen Hotel Berisiko Menengah Tinggi
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/arena-permainan.html" title="Arena Permainan">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/610/76a/19b/61076a19b32a9842767362.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Arena Permainan">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Arena Permainan
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/dermaga-marina.html" title="Dermaga Marina">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/610/769/c6e/610769c6eefa9253283908.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Dermaga Marina">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Dermaga Marina
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/fasilitasi-gelanggang-arena-bungee-jumping.html" title="Fasilitasi Gelanggang /Arena Bungee Jumping">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/637/c30/83a/637c3083a6090915076179.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Fasilitasi Gelanggang /Arena Bungee Jumping">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Fasilitasi Gelanggang /Arena Bungee Jumping
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/fasilitasi-gelanggangarena-bowling.html" title="Fasilitasi Gelanggang/Arena Bowling">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/610/76b/f61/61076bf611967063129547.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Fasilitasi Gelanggang/Arena Bowling">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Fasilitasi Gelanggang/Arena Bowling
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/fasilitasi-gelanggangarena-paralayang-paragliding-dan-layang-gantung-hang-gliding.html" title="Fasilitasi Gelanggang/Arena Paralayang (Paragliding) Dan Layang Gantung (Hang Gliding)">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/635/600/84d/63560084defb0829676761.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Fasilitasi Gelanggang/Arena Paralayang (Paragliding) Dan Layang Gantung (Hang Gliding)">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Fasilitasi Gelanggang/Arena Paralayang (Paragliding) Dan Layang Gantung (Hang Gliding)
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/fasilitasi-pengelolaan-gelanggangarena-slingshot.html" title="Fasilitasi Pengelolaan Gelanggang/Arena Slingshot">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/635/603/24e/63560324e7748854607036.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Fasilitasi Pengelolaan Gelanggang/Arena Slingshot">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Fasilitasi Pengelolaan Gelanggang/Arena Slingshot
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/hotel-bintang-berisiko-tinggi.html" title="Hotel Bintang Berisiko Tinggi">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/636/0b2/e5d/6360b2e5d55f6382617101.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Hotel Bintang Berisiko Tinggi">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Hotel Bintang Berisiko Tinggi
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/hotel-melati-berisiko-tinggi.html" title="Hotel Melati Berisiko Tinggi">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/638/5d7/3cc/6385d73cc1f60843920212.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Hotel Melati Berisiko Tinggi">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Hotel Melati Berisiko Tinggi
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/kawasan-pariwisata.html" title="Kawasan Pariwisata">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/610/769/5cc/6107695cc8654364068975.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Kawasan Pariwisata">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Kawasan Pariwisata
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/klub-malam.html" title="Klub Malam">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/637/c33/c4f/637c33c4f3824587321863.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Klub Malam">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Klub Malam
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/pengelolaan-goa.html" title="Pengelolaan Goa">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/610/76d/8f8/61076d8f8ee2e899343004.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Pengelolaan Goa">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Pengelolaan Goa
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/restoran-berisiko-menengah-tinggi.html" title="Restoran Berisiko Menengah Tinggi">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/635/3bb/a28/6353bba28d00d402752669.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Restoran Berisiko Menengah Tinggi">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Restoran Berisiko Menengah Tinggi
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/sante-par-aqua-spa-tirta-1.html" title="Sante Par Aqua (Spa) Tirta 1">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/610/76d/4cd/61076d4cdbb0d453971424.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Sante Par Aqua (Spa) Tirta 1">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Sante Par Aqua (Spa) Tirta 1
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/sante-par-aqua-spa-tirta-3.html" title="Sante par Aqua (Spa) Tirta 3">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/637/c33/23e/637c3323e075d322920251.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Sante par Aqua (Spa) Tirta 3">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Sante par Aqua (Spa) Tirta 3
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/vila-bintang-2-gold.html" title="Vila Bintang 2 (Gold)">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/635/3a5/438/6353a54380803615397866.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Vila Bintang 2 (Gold)">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Vila Bintang 2 (Gold)
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/wisata-arung-jeram.html" title="Wisata Arung Jeram">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/610/76d/66a/61076d66ae1ca593638713.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Wisata Arung Jeram">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Wisata Arung Jeram
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/wisata-petualangan-alam.html" title="Wisata Petualangan Alam">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/610/76e/33b/61076e33be17b565165449.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Wisata Petualangan Alam">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Wisata Petualangan Alam
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-md-3">
                                                    <a class="d-flex flex-row align-items-center gap-2 dropdown-item" href="usaha-tersertifikasi/wisata-tirta-lainnya.html" title="Wisata Tirta Lainnya">
                                                        <div class="flex-0">
                                                            <img class="" src="{{asset('uploads/public/610/863/7a4/6108637a42460000555499.png')}}" style="height: 1.6rem; width: 1.6rem;" alt="Wisata Tirta Lainnya">
                                                        </div>
                                                        <span class="text-truncate">
                                                            Wisata Tirta Lainnya
                                                        </span>
                                                    </a>
                                                </div>
                                                <!-- End Col -->
                                            </div>
                                            <!-- End Row -->
                                        </div>
                                    </div>
                                    <!-- End Col -->
                                </div>
                                <!-- End Row -->
                            </div>
                            <!-- End Mega Menu -->
                        </li>
                        <!-- End Landings -->
                        <!-- Company -->
                        <li class="hs-has-sub-menu nav-item">
                            <a id="riskMegaMenu" class="hs-mega-menu-invoker nav-link dropdown-toggle text-navbar-custom" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Standardisasi</a>
                            <!-- Mega Menu -->
                            <div class="hs-sub-menu dropdown-menu" aria-labelledby="riskMegaMenu" style="min-width: 40rem;">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <a href="unsur-standar/resiko-rendah.html" class="dropdown-item">
                                            <div class="d-flex flex-row align-items-center gap-3">
                                                <div class="flex-1">
                                                    <img class="" style="width: 2rem;" src="{{asset('uploads/public/610/6bf/82b/6106bf82bfa1b008676610.png')}}" alt="Risiko Rendah">
                                                </div>
                                                <div class="d-flex flex-column">
                                                    <span class="fs-5 fw-semibold text-primary">Risiko Rendah</span>
                                                    <small class="fs-6 text-gray">Pustaka Standarisasi</small>
                                                </div>
                                            </div>
                                        </a>
                                        <a href="unsur-standar/resiko-menengah-rendah.html" class="dropdown-item">
                                            <div class="d-flex flex-row align-items-center gap-3">
                                                <div class="flex-1">
                                                    <img class="" style="width: 2rem;" src="{{asset('uploads/public/610/6bf/a60/6106bfa60e330763331728.png')}}" alt="Risiko Menengah Rendah">
                                                </div>
                                                <div class="d-flex flex-column">
                                                    <span class="fs-5 fw-semibold text-primary">Risiko Menengah Rendah</span>
                                                    <small class="fs-6 text-gray">Pustaka Standarisasi</small>
                                                </div>
                                            </div>
                                        </a>
                                        <a href="unsur-standar/resiko-menengah-tinggi.html" class="dropdown-item">
                                            <div class="d-flex flex-row align-items-center gap-3">
                                                <div class="flex-1">
                                                    <img class="" style="width: 2rem;" src="{{asset('uploads/public/610/6bf/9da/6106bf9da32a7528068023.png')}}" alt="Risiko Menengah Tinggi">
                                                </div>
                                                <div class="d-flex flex-column">
                                                    <span class="fs-5 fw-semibold text-primary">Risiko Menengah Tinggi</span>
                                                    <small class="fs-6 text-gray">Pustaka Standarisasi</small>
                                                </div>
                                            </div>
                                        </a>
                                        <a href="unsur-standar/resiko-tinggi.html" class="dropdown-item">
                                            <div class="d-flex flex-row align-items-center gap-3">
                                                <div class="flex-1">
                                                    <img class="" style="width: 2rem;" src="{{asset('uploads/public/610/6bf/79d/6106bf79d6d5c677394402.png')}}" alt="Risiko Tinggi">
                                                </div>
                                                <div class="d-flex flex-column">
                                                    <span class="fs-5 fw-semibold text-primary">Risiko Tinggi</span>
                                                    <small class="fs-6 text-gray">Pustaka Standarisasi</small>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <!-- Banner Image -->
                                    <div class="col-lg-4 d-none d-lg-block">

                                        <!-- Banner Image -->
                                        <div class="navbar-dropdown-menu-banner bg-soft-primary">
                                            <div class="navbar-dropdown-menu-banner-content">
                                                <a class="" href="#">
                                                    <div class="d-flex flex-column w-100 mt-2 me-2">
                                                        <div class="position-relative">
                                                            <img class="w-100 rounded mb-3" src="{{asset('themes/sisupar/assets/landing/img/others/bg-menu-standarisasi.png')}}" alt="BG Standarisasi">
                                                        </div>
                                                        <small class="text-secondary fs-6">Standarisasi usaha
                                                            berdasarkan kategori resiko.</small>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- End Banner Image -->


                                    </div>
                                    <!-- End Banner Image -->
                                </div>
                            </div>
                            <!-- End Mega Menu -->
                        </li>
                        <!-- End Company -->

                        <li class="nav-item">
                            <a class="nav-link text-navbar-custom" href="daftar-lsup.html">Lembaga Sertifikasi</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link text-navbar-custom" href="hubungi-kami.html">Hubungi Kami</a>
                        </li>

                        <li class="nav-item">
                            <a class="btn btn-sm rounded-pill btn-light btn-transition" href="{{url('/login')}}">
                                Login
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- End Collapse -->
        </nav>
    </div>
</header>