<nav class="sidebar">
    <div class="logo d-flex justify-content-between">
        <div class="brand_text text-center">
            <h1>Walikukun</h1>
            <div class="h3">Sport Center</div>
        </div>
        <div class="sidebar_close_icon d-lg-none">
            <i class="ti-close"></i>
        </div>
    </div>
    <ul id="sidebar_menu">
        <li class="mm-active">
            <a class="has-arrow" href="#" aria-expanded="false">

                <img src="img/menu-icon/dashboard.svg" alt>
                <span>Dashboard</span>
            </a>
        </li>
        <li class="{{ Request::is('/laporan') ? 'mm-active' : '' }}">
            <a class="has-arrow" href="{{url('/laporan')}}" aria-expanded="false">
                <i class='bx bxs-file bx-sm'></i>
                <span>Laporan</span>
            </a>
        </li>
    </ul>
</nav>