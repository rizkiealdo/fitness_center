<nav class="sidebar">
    <div class="logo d-flex justify-content-between">
        <a href="#">
            <div class="brand_text text-center">
                <h1>Walikukun</h1>
                <div class="h3">Sport Center</div>
            </div>
        </a>
        <div class="sidebar_close_icon d-lg-none">
            <i class="ti-close"></i>
        </div>
    </div>
    <ul id="sidebar_menu">
        <li class="{{ Request::is('admin/dashboard') ? 'mm-active' : '' }}">
            <a class="has-arrow" href="{{url('/admin/dashboard')}}" aria-expanded="false">

                <i class='bx bxs-dashboard bx-sm'></i>
                <span>Dashboard</span>
            </a>
        </li>
        <li class="{{ Request::is('admin/operator') ? 'mm-active' : '' }}">
            <a class="has-arrow" href="{{url('/admin/operator')}}" aria-expanded="false">
                <i class='bx bxs-slideshow bx-sm'></i>
                <span>Operator</span>
            </a>
        </li>
        <li class="{{ Request::is('admin/history') ? 'mm-active' : '' }}">
            <a class="has-arrow" href="{{url('/admin/history')}}" aria-expanded="false">
                <i class='bx bx-history bx-sm'></i>
                <span>History Aktivitas</span>
            </a>
        </li>
        <li class="{{ Request::is('admin/data-customer') ? 'mm-active' : '' }}">
            <a class="has-arrow" href="{{url('/admin/data-customer')}}" aria-expanded="false">
                <i class='bx bxs-user-pin bx-sm'></i>
                <span>Data Customer</span>
            </a>
        </li>
        <li class="{{ Request::is('admin/data-alat') ? 'mm-active' : '' }}">
            <a class="has-arrow" href="{{url('/admin/data-alat')}}" aria-expanded="false">
                <i class='bx bx-cog bx-sm'></i>
                <span>Data Alat</span>
            </a>
        </li>
        <li class="{{ Request::is('admin/data-teknisi') ? 'mm-active' : '' }}">
            <a class="has-arrow" href="{{url('/admin/data-teknisi')}}" aria-expanded="false">
                <i class='bx bxs-user-voice bx-sm'></i>
                <span>Data Teknisi</span>
            </a>
        </li>
        <li class="{{ Request::is('admin/data-omset') ? 'mm-active' : '' }}">
            <a class="has-arrow" href="{{url('/admin/data-omset')}}" aria-expanded="false">
                <i class='bx bxs-calculator bx-sm'></i>
                <span>Data Omset</span>
            </a>
        </li>
        <li class="{{ Request::is('/laporan') ? 'mm-active' : '' }}">
            <a class="has-arrow" href="{{url('/laporan')}}" aria-expanded="false">
                <i class='bx bxs-file bx-sm'></i>
                <span>Laporan</span>
            </a>
        </li>
    </ul>
</nav>