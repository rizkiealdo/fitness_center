<nav class="sidebar">
    <div class="logo d-flex justify-content-between">
        <div class="brand_text text-center">
            <h1>Walikukun</h1>
            <div class="h3">Sport Center</div>
        </div>
        <div class="sidebar_close_icon d-lg-none">
            <i class="ti-close"></i>
        </div>
    </div>
    <ul id="sidebar_menu">
        <li class="{{ Request::is('manager/dashboard') ? 'mm-active' : '' }}">
            <a class="has-arrow" href="{{url('/manager/dashboard')}}" aria-expanded="false">

                <i class='bx bxs-dashboard bx-sm'></i>
                <span>Dashboard</span>
            </a>
        </li>
        <li class="{{ Request::is('manager/manajemen-akun') ? 'mm-active' : '' }}">
            <a class="has-arrow" href="{{url('/manager/manajemen-akun')}}" aria-expanded="false">
                <i class='bx bxs-user-check bx-sm'></i>
                <span>Manajemen Akun</span>
            </a>
        </li>
        <li class="{{ Request::is('manager/manajemen-harga') ? 'mm-active' : '' }}">
            <a class="has-arrow manajemen-harga" href="{{url('/manager/manajemen-harga')}}" aria-expanded="false">
                <i class='bx bx-money-withdraw bx-sm'></i>
                <span>Manajemen Harga</span>
            </a>
        </li>
        <li class="{{ Request::is('/laporan') ? 'mm-active' : '' }}">
            <a class="has-arrow" href="{{url('/laporan')}}" aria-expanded="false">
                <i class='bx bxs-file bx-sm'></i>
                <span>Laporan</span>
            </a>
        </li>
    </ul>
</nav>