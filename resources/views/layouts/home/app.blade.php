<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>{{ $title ?? config('app.name') }} | {{ config('app.name') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Boilerplate Laravel System" name="description" />
    <meta content="Phicosdev" name="author" />
    <meta content="{{ url('/') }}/" name="base_url" />
    <meta content="{{ asset('') }}" name="asset_url">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="robots" content="" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Sertifikasi dan Standardisasi Usaha Pariwisata" />
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Sertifikasi dan Standardisasi Usaha Pariwisata">

    <link rel="shortcut icon" type="image/x-icon"
        href="{{asset('themes/sisupar/assets/landing/img/others/favicon.ico')}}">
    <meta name="turbo-visit-control" content="disable" />

    <!-- Google Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com/">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css"
        integrity="sha512-q3eWabyZPc1XTCmF+8/LuE1ozpg5xxn7iO89yfSOd5/oKvyqLngoNGsx8jq92Y8eXJ/IRxQbEC+FGSYxtk2oiw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link
        href="https://fonts.googleapis.com/css2?family=Exo:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap"
        rel="stylesheet">

    <!-- CSS Implementing Plugins -->
    <link href="{{asset('themes/combine/bd32f8be9002fcb3b6d870bf3155d145-1676218024.css')}}" rel="stylesheet">
    <link href="{{asset('themes/sisupar/assets/js/sweetalert/sweetalert2.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('themes/sisupar/assets/js/social-sharing/css/socialSharing.css')}}" rel="stylesheet"
        type="text/css">
    <link href="{{asset('themes/sisupar/assets/js/social-sharing/main.scss')}}" rel="stylesheet" type="text/css">
</head>

<body>
    @include('layouts.component.home.header.list')

    <main id="content" role="main">
        @yield('content')
    </main>

    @include('layouts.component.home.footer.list')

    @include('layouts.component.home.secondary-content.list')

    <!-- JS Global Compulsory Scripts -->
    <script src="{{asset('themes/combine/1ba03d4012871238252509050eb6257f-1666765792')}}"></script>

    <script src="{{asset('themes/sisupar/assets/landing/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
    <!--  JS Implementing Plugins -->
    <script src="{{asset('themes/combine/c4d6ca79e5ddff78ee8e0e348f01e3a7-1666765793')}}"></script>

    <script src="{{asset('themes/sisupar/assets/landing/vendor/typed.js/lib/typed.min.js')}}"></script>

    <!-- JS Front -->
    <script src="{{asset('themes/combine/4344b2860f479e04770092bd80da76f6-1666765792')}}"></script>
    <script src="{{asset('themes/sisupar/assets/js/sweetalert/sweetalert2.min.js')}}"></script>
    <script src="{{asset('themes/sisupar/assets/js/social-sharing/js/socialSharing.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.10/clipboard.min.js"></script>
    <!-- JS Plugins Init. -->
    <script>
        (function() {
            // INITIALIZATION OF HEADER
            // =======================================================
            new HSHeader('#header').init()


            // INITIALIZATION OF MEGA MENU
            // =======================================================
            new HSMegaMenu('.js-mega-menu', {
                desktop: {
                    position: 'left'
                }
            })


            // INITIALIZATION OF SHOW ANIMATIONS
            // =======================================================
            new HSShowAnimation('.js-animation-link')


            // INITIALIZATION OF BOOTSTRAP VALIDATION
            // =======================================================
            HSBsValidation.init('.js-validate', {
                onSubmit: data => {
                    data.event.preventDefault()
                }
            })


            // INITIALIZATION OF BOOTSTRAP DROPDOWN
            // =======================================================
            HSBsDropdown.init()


            // INITIALIZATION OF GO TO
            // =======================================================
            new HSGoTo('.js-go-to')

            // INITIALIZATION OF AOS
            // =======================================================
            AOS.init({
                duration: 650,
                once: true
            });

            // INITIALIZATION OF SWIPER
            // =======================================================


            // Card Grid
            var swiper = new Swiper('.js-swiper-card-blocks', {
                slidesPerView: 1,
                pagination: {
                    el: '.js-swiper-card-blocks-pagination',
                    dynamicBullets: true,
                    clickable: true,
                },
                autoplay: {
                    delay: 2000,
                },
                breakpoints: {
                    620: {
                        slidesPerView: 1,
                        spaceBetween: 15,
                    },
                    1024: {
                        slidesPerView: 2,
                        spaceBetween: 15,
                    },
                },
            });

            // INITIALIZATION OF TEXT ANIMATION (TYPING)
            // =======================================================
            HSCore.components.HSTyped.init('.js-typedjs')

            // INITIALIZATION OF SELECT
            // =======================================================
            HSCore.components.HSTomSelect.init('.js-select')

            // INITIALIZATION OF VIDEO PLAYER
            // =======================================================
            new HSVideoPlayer('.js-inline-video-player')


            var clipboard = new ClipboardJS('.copy-js');

            clipboard.on('success', function(e) {
                console.info('Action:', e.action);
                console.info('Text:', e.text);
                console.info('Trigger:', e.trigger);

                e.clearSelection();
            });

        })()
    </script>
    <script type="text/javascript">
        (function() {
            // INITIALIZATION OF SWIPER
            // =======================================================
            var slidesPerView = new Swiper('.js-swiper-slides', {
                slidesPerView: 6,
                spaceBetween: 10,
                loop: true,
                autoplay: {
                    delay: 1000,
                    disableOnInteraction: false,
                },
            });
        })()
    </script>
    <script src="{{asset('themes/modules/assets/js/framework-extras.js')}}"></script>
    <link rel="stylesheet" property="stylesheet" href="{{asset('themes/modules/assets/css/framework-extras.css')}}">
    <script>
        var pageId = "home";
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();

            if (pageId == 'home') {
                //>=, not <=
                if (scroll >= 100) {
                    //clearHeader, not clearheader - caps H
                    $(".header-scroll").addClass("bg-primary");
                } else {
                    $(".header-scroll").removeClass("bg-primary");
                }
            }

        }); //missing );
    </script>
</body>

</html>